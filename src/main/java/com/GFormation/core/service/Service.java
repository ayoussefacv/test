package com.GFormation.core.service;

import java.util.List;


public interface Service<T> {

	T getById(long id);
	List<T> getAll();
	
	T add(T t);
	void update(T t);
    void deleteById(long id);
    void delete(T t);
    

}
