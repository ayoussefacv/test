package com.GFormation.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import com.GFormation.core.dao.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;


@Transactional
public abstract class ServiceImpl<T,DaoT extends DAO<T>> implements com.GFormation.core.service.Service<T> {


   @Autowired
   SessionFactory sessionFactory;
	@Autowired
	private DaoT dao;

	public T add(T t) {
		return  dao.add(t);		
	}

	public void update(T t) {
		dao.update(t);
	}

	public T getById(long id) {
		return  dao.getById(id);
	}

	public void deleteById(long id) {
		dao.deleteById(id);
	}
	public void delete(T t) {
		dao.delete(t);
	}

	public List<T> getAll() {
		return dao.getAll();
	}

   public Session getCurrentSession(){
      return sessionFactory.getCurrentSession();
   }
}
