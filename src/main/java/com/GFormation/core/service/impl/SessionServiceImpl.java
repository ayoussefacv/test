package com.GFormation.core.service.impl;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.GFormation.core.dao.DAO;
import com.GFormation.core.dao.SessionCollaboratorDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.model.User;
import com.GFormation.core.service.SessionService;
import com.GFormation.core.util.Auth;

@Transactional
public class SessionServiceImpl implements SessionService{
	
	@Autowired
	UserDao userDao;
	@Autowired
	SessionDao sessionDao;
	@Autowired
	SessionCollaboratorDao sessionCollaboratorDao;

   @Autowired
   SessionFactory sessionFactory;

   protected final org.hibernate.Session getCurrentSession(){
      return sessionFactory.getCurrentSession();
   }
	@Override
	public boolean addCollaboratorToSession(long collaboratorId, long sessionId) {

		User collaborator = userDao.getById(collaboratorId);
			System.out.println("collaborator="+collaborator);
		Session session = sessionDao.getById(sessionId);
			System.out.println("session="+session);
			
		SessionCollaborator	sessionCollaborator =new SessionCollaborator();
		sessionCollaborator.setSession(session);
		sessionCollaborator.setCollaborator(collaborator);
		sessionCollaborator.setConfirmationCode(Auth.generateConfirmationCode(collaborator.getEmail(), session.getId()));
		System.out.println("session.getCollaborators()="+session.getCollaborators().toString());
		
		session.getSessionCollaborators().add(sessionCollaborator);
		//sessionCollaboratorDao.add(sessionCollaborator);
		sessionDao.update(session);
		return true;
	}

	@Override
	@Transactional
	public boolean removeCollaborator(int sessionId , int collaboratorId) {

		User collaborator = userDao.getById(collaboratorId);
			System.out.println("collaborator="+collaborator);
		Session session = sessionDao.getById(sessionId);
			System.out.println("session="+session);
		if(collaborator!=null && session != null )
		{
			SessionCollaborator	sessionCollaborator =new SessionCollaborator();
			sessionCollaborator.setSession(session);
			sessionCollaborator.setCollaborator(collaborator);
			//session.echo();
			session.removeCollaborator(collaborator);
			//System.out.println("##### =====> \n session.toString()="+sessionCollaborator.getSession().getId());
			sessionCollaboratorDao.delete(sessionCollaborator);
			//sessionDao.add(session);
			//session.echo();	
			return true;
		}
		return false;
	}

	@Override
	public boolean addTrainerToSession(long trainerId, long sessionId) {

		User trainer = userDao.getById(trainerId);
			System.out.println("collaborator="+trainer);
		Session session = sessionDao.getById(sessionId);
			System.out.println("session="+session);
		if(trainer != null && session!= null)
		{
			session.setTrainer(trainer);
			sessionDao.update(session);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeTrainer(int sessionId) {

		Session session = sessionDao.getById(sessionId);
			System.out.println("session="+session);
		if(session!= null)
		{
			session.setTrainer(null);
			sessionDao.update(session);
			return true;
		}
		return false;
		
	}

	@Override
	public  List<Session> getTrainerSessions(long trainerId) {
    	
    	Query query =  getCurrentSession().createSQLQuery("select * from Session where trainer_id=?")
    			.addEntity(Session.class)
    			.setLong(0,trainerId);
    	List<Session>  sessions = query.list();
    	System.out.println("list==="+query.list().toString());
    	return sessions;
	}
	@Override
	public  List<Session> getCollaboratorSessions(long collaboratorId) {
    	
    	Query query =  getCurrentSession().
    	createSQLQuery("select * from  session  JOIN Session_collaborators ON session_id = id "
    			+ " where collaborator_id=?")
    			.addEntity(Session.class)
    			.setLong(0,collaboratorId);
    	List<Session>  sessions = query.list();
    	System.out.println("list==="+query.list().toString());
    	return sessions;
	}



}