package com.GFormation.core.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Document;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.User;
import com.GFormation.core.service.DocumentService;
import com.GFormation.core.util.Auth;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class DocumentController{

	  @Autowired
	  private DocumentService documentService;
	  @Autowired
	  private SessionDao sessionDao;
	  @Autowired
	  private UserDao userDAO;

	  @RequestMapping("/")
	  public String home() {
	
	    return "fileUploader";
	  }

  @RequestMapping(value = "/upload/{sessionId}", method = RequestMethod.POST)
  public @ResponseBody List<Document> preUpload(MultipartHttpServletRequest request, @PathVariable("sessionId") long sessionId ,
      HttpServletResponse response) throws IOException {
	  
	  Session session = sessionDao.getById(sessionId);

	  if(session != null ) 
	  if(  Auth.getUser(userDAO).getId().equals(session.getTrainer().getId())) // verify if this trainer is the owner
      {
    	  return this.upload(request, session, response);
      }
		return null;

  }
	  
  private List<Document> upload(MultipartHttpServletRequest request, Session session,
		      HttpServletResponse response) throws IOException {

	    // Getting uploaded files from the request object
	    Map<String, MultipartFile> fileMap = request.getFileMap();
	
	    // Maintain a list to send back the files info. to the client side
	    List<Document> uploadedFiles = new ArrayList<Document>();
	
	    Set<Document> sessionDocuments = session.getDocuments();
	    // Iterate through the map
	    for (MultipartFile multipartFile : fileMap.values()) {
	    	
		
		  Document fileInfo = getDocumentInfo(multipartFile);
	      fileInfo.setSession(session);
	      
	      if(!documentService.isExist(fileInfo, session))
	    	{
		      // Save the file to local disk
		      saveFileToLocalDisk(multipartFile);
		      
		      // Save the file info to database
		      fileInfo = saveFileToDatabase(fileInfo);
		      
		      // adding the file info to the list
		      uploadedFiles.add(fileInfo);
	    		
	    	}
	    }
	
	    // 2. Set response type to json
	    response.setContentType("application/json");
	
	    // 3. Convert List<FileMeta> into JSON format
	    ObjectMapper mapper = new ObjectMapper();
	
	    // 4. Send resutl to client
	    mapper.writeValue(response.getOutputStream(), uploadedFiles);
	    
	    return uploadedFiles;
  }


  @RequestMapping(value = {"/list"})
  public String listBooks(Map<String, Object> map) {

    map.put("fileList", documentService.listFiles());

    return "/listFiles";
  }

  @RequestMapping(value = "/get/{fileId}", method = RequestMethod.GET)
  public void getFile(HttpServletResponse response, @PathVariable Long fileId) {

    Document dataFile = documentService.getFile(fileId);

     Session session = dataFile.getSession();
	  
	if(session != null ) 
	if( true) //session.getCollaborators().contains(Auth.getUser(userDAO))) // verify if this trainer is the owner
	{
	    File file = new File(dataFile.getLocation(), dataFile.getNameInDisk());
	
	    try {
	      response.setContentType(dataFile.getType());
	      response.setHeader("Content-disposition", "attachment; filename=\"" + dataFile.getName()
	          + "\"");
	
	      FileCopyUtils.copy(FileUtils.readFileToByteArray(file), response.getOutputStream());
	
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	}
  }

 //================================================================================================= private functions
  private void saveFileToLocalDisk(MultipartFile multipartFile) throws IOException,
      FileNotFoundException {

    String outputFileName = getOutputFilename(multipartFile);

    FileCopyUtils.copy(multipartFile.getBytes(), new FileOutputStream(outputFileName));
  }

  private Document saveFileToDatabase(Document uploadedFile) {

    return documentService.saveFile(uploadedFile);

  }

  private String getOutputFilename(MultipartFile multipartFile) {

    return getDestinationLocation() + generateFileNameInDisk(multipartFile);
  }

  private Document getDocumentInfo(MultipartFile multipartFile) throws IOException {

    Document fileInfo = new Document();
  	 	
    fileInfo.setName(multipartFile.getOriginalFilename());
    fileInfo.setNameInDisk(this.generateFileNameInDisk(multipartFile));
    fileInfo.setSize(multipartFile.getSize());
    fileInfo.setType(multipartFile.getContentType());
    fileInfo.setLocation(getDestinationLocation());

    return fileInfo;
  }

  private String generateFileNameInDisk(MultipartFile multipartFile) {
	  java.util.Date date= new java.util.Date();
	 	String fileName= FilenameUtils.removeExtension(multipartFile.getOriginalFilename());
	 	String extension=multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
	 	return fileName+"_"+date.getTime()+extension;
  }
  private String getDestinationLocation() {
    return "E:/tmp/files/";
  }
  
}
