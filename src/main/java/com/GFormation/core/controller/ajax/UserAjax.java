package com.GFormation.core.controller.ajax;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.controller.AdminController;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Training;
import com.GFormation.core.model.User;
import com.GFormation.core.service.SessionService;


@Controller
@RequestMapping(value="/ajax/user")
public class UserAjax {
	private static final Logger logger =LoggerFactory.getLogger(UserAjax.class);

	@Autowired
	UserDao userDAO;

	@Autowired
	SessionService sessionService;

	//============================================================================================= get user
	@RequestMapping("/{id}")
    public ModelAndView show(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		User user = this.userDAO.getById(id);
		if(user != null)
		{
			modelAndView.setViewName("ajax/user/show");
			modelAndView.addObject("user", user);
	        return modelAndView;
		}
		modelAndView.setViewName("error");
		modelAndView.addObject("message", "connexion_error");
        return modelAndView;
    }
	//============================================================================================= list
    @RequestMapping(value="/search", method = RequestMethod.POST)
	public ModelAndView list( @RequestParam String username){
		
    	System.out.println("entering ajax/user/search");

		ModelAndView modelAndView = new ModelAndView("ajax/user/list");
		
		List<User> users = userDAO.searchByUsername(username , 0  , 5);
		System.out.println("username="+username);
		System.out.println("users="+users);
		modelAndView.addObject("users", users);
		return modelAndView;
	} 
	//============================================================================================= addCollaboratorToSession
    @RequestMapping(value="/addToSession", method = RequestMethod.POST)
	public ModelAndView addCollaboratorToSession( @RequestParam("sessionId") long sessionId , @RequestParam("collaboratorId") long collaboratorId){
		
    	System.out.println("entering ajax/user/addToSession");

		ModelAndView modelAndView = new ModelAndView("ajax/message");
		System.out.println("collaboratorId="+collaboratorId);
		System.out.println("sessionId="+sessionId);
		
		boolean success = sessionService.addCollaboratorToSession(collaboratorId , sessionId );
		modelAndView.addObject("message", "OK : user added to session successfully");
		return modelAndView;
	} 
	//============================================================================================= addTrainerToSession
    @RequestMapping(value="/addTrainerToSession", method = RequestMethod.POST)
	public ModelAndView addTrainerToSession( @RequestParam("sessionId") long sessionId , @RequestParam("collaboratorId") long trainerId){
		
    	System.out.println("### entering ajax/user/addToSession");

		ModelAndView modelAndView = new ModelAndView("ajax/message");
		System.out.println("trainerId="+trainerId);
		System.out.println("sessionId="+sessionId);
		
		boolean success = sessionService.addTrainerToSession(trainerId , sessionId );
		if(success == true)
			modelAndView.addObject("message", "OK : trainer added to session successfully");
		else
			modelAndView.addObject("message", "error : cannot add trainer to session");
		return modelAndView;
	} 
    
	//============================================================================================= remove trainer from Session
    @RequestMapping(value="/removeCollaboratorFromSession", method = RequestMethod.POST)
    public ModelAndView removeTrainerFromSession(@RequestParam("sessionId") int sessionId){
        
    	ModelAndView modelAndView = new ModelAndView("ajax/message");
    	boolean success = sessionService.removeTrainer(sessionId);
		if(success == true)
			modelAndView.addObject("message", "OK : trainer removed from session successfully");
		else
			modelAndView.addObject("message", "error : cannot remove trainer from session");
		return modelAndView;
    }
}
