package com.GFormation.core.controller.tmp;


import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.GFormation.core.domain.FileMeta;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class FileController {
 
    LinkedList<FileMeta> files = new LinkedList<FileMeta>();
    FileMeta fileMeta = null;
    /***************************************************
     * URL: /rest/controller/upload  
     * upload(): receives files
     * @param request : MultipartHttpServletRequest auto passed
     * @param response : HttpServletResponse auto passed
     * @return LinkedList<FileMeta> as json format
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonGenerationException 
     ****************************************************/
    @RequestMapping(value="/rest/controller/upload", method = RequestMethod.POST)
    public @ResponseBody LinkedList<FileMeta> upload(MultipartHttpServletRequest request, HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException {

      	 
        //1. build an iterator
         Iterator<String> itr =  request.getFileNames();
         MultipartFile mpf = null;
         //2. get each file
         while(itr.hasNext()){
 
             //2.1 get next MultipartFile
             mpf = request.getFile(itr.next()); 
             System.out.println(mpf.getOriginalFilename() +" uploaded! "+files.size());
 
             //2.2 if files > 10 remove the first from the list
             if(files.size() >= 10)
                 files.pop();
 
             //2.3 create new fileMeta
             fileMeta = new FileMeta();
             fileMeta.setFileName(mpf.getOriginalFilename());
             fileMeta.setFileSize(mpf.getSize()/1024+" Kb");
             fileMeta.setFileType(mpf.getContentType());
             
             System.out.println("pt1");
             try {
                fileMeta.setBytes(mpf.getBytes());

                 // copy file to local disk (make sure the path "e.g. D:/temp/files" exists)  
       	   	 	java.util.Date date= new java.util.Date();

       	   	 	String fileName= FilenameUtils.removeExtension(mpf.getOriginalFilename());
       	   	 	String extension=mpf.getOriginalFilename().substring(mpf.getOriginalFilename().lastIndexOf("."));
                FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("E:/tmp/files/"+fileName+"_"+date.getTime()+extension));

                 System.out.println("pt2");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
             //2.4 add to files
             files.add(fileMeta);
         }
        // result will be like this
        // [{"fileName":"app_engine-85x77.png","fileSize":"8 Kb","fileType":"image/png"},...]*

         System.out.println("pt3");
        // System.out.println(files.toArray().toString());

         // 2. Set response type to json
         response.setContentType("application/json");
  
         // 3. Convert List<FileMeta> into JSON format
         ObjectMapper mapper = new ObjectMapper();
  
         // 4. Send resutl to client
         mapper.writeValue(response.getOutputStream(), files);
        return files;
    }
    /***************************************************
     * URL: /rest/controller/get/{value}
     * get(): get file as an attachment
     * @param response : passed by the server
     * @param value : value from the URL
     * @return void
     ****************************************************/
    @RequestMapping(value = "/rest/controller/get/{value}", method = RequestMethod.GET)
    public void get(HttpServletResponse response,@PathVariable String value){
        FileMeta getFile = files.get(Integer.parseInt(value));
        try {      
            System.out.println("pt3");
               response.setContentType(getFile.getFileType());
               response.setHeader("Content-disposition", "attachment; filename=\""+getFile.getFileName()+"\"");
               FileCopyUtils.copy(getFile.getBytes(), response.getOutputStream());
               System.out.println("pt4");
        }catch (IOException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
        }
    }
    @RequestMapping(value = "/get")
    public String  get(Model model){
    	return "index";
    }
}