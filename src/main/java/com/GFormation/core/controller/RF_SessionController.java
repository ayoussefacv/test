package com.GFormation.core.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.model.Training;
import com.GFormation.core.model.TrainingStatus;
import com.GFormation.core.model.User;
import com.GFormation.core.service.SessionService;
import com.GFormation.core.util.Auth;
import com.GFormation.core.util.Editor;
import com.GFormation.core.util.SpringMailSender;
import com.GFormation.core.util.Validator;


@Controller
@RequestMapping(value="RF")
public class RF_SessionController {
	private static final Logger logger =LoggerFactory.getLogger(RF_SessionController.class);

	@Autowired
	TrainingDao trainingDAO;
	@Autowired
	SessionDao sessionDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;
	@Autowired
	SessionService sessionService;
	@Autowired
	UserDao userDAO;

	//============================================================================================= show session
	@RequestMapping("sessions/{id}")
    public ModelAndView show(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		Session session = this.sessionDAO.getById(id);
		if(session != null)
		{
			modelAndView.setViewName("RF/session/show");
			modelAndView.addObject("session", session);
	        return modelAndView;
		}
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "no_session_found_with_this_id");
        return modelAndView;
    }

	//============================================================================================= add session
			// see RF_TrainingController
	//============================================================================================= update session
	@RequestMapping("sessions/{session_id}/update")
    public ModelAndView update(@PathVariable("session_id") int session_id){

		ModelAndView modelAndView = new ModelAndView("RF/session/update");
		modelAndView.addObject("session", this.sessionDAO.getById(session_id));
    	System.out.println(this.sessionDAO.getById(session_id));
        return modelAndView;
    }
	
    @RequestMapping(value={"sessions/{session_id}/update"} , method = RequestMethod.POST)
    public String update( @PathVariable long session_id , @ModelAttribute("session") Session s, @RequestParam String date1,@RequestParam String time1, @RequestParam String time2){

    	if( Validator.session(s, date1, time1, time2))
    	{
	        if(s!= null)
	        {
		        sessionDAO.update(s);
		        //training.getSessions().add(s);
		        //trainingDAO.update(training);
		        return "redirect:/RF/sessions/"+session_id+"";
	        }
    	}
        return "redirect:/RF/sessions/"+s.getId()+"/update";
    }
	//============================================================================================= delete session
    @RequestMapping(value="sessions/{id}/delete", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable("id") int id){
        Session session = sessionDAO.getById(id) ;
    	long training_id =session.getTraining().getId();
    	//sessionDAO.deleteById(id);
    	System.out.println("eeeee");
    	Training training = trainingDAO.getById(training_id);
    	if(session.getCollaborators()!=null && session.getStatus() != TrainingStatus.Open)
    	{
    		training.getSessions().remove(sessionDAO.getById(id)) ;//deleteById(id);
        	trainingDAO.update(training);
        	return new ModelAndView("ajax/message","message","done");
    	}
    	else
    	{
        	return new ModelAndView("ajax/message","message","cannot delete session because it is already in open status and have collaborators");
    	}
    }
	//============================================================================================= search list collaborators
    @RequestMapping(value="sessions/search", method = RequestMethod.POST)
	public ModelAndView searchListCollaborators( @RequestParam String username){
		
    	System.out.println("entering ajax/user/search");

		ModelAndView modelAndView = new ModelAndView("ajax/user/list");
		
		List<User> users = userDAO.searchByUsername(username , 0  , 5);
		System.out.println("username="+username);
		System.out.println("users="+users);
		modelAndView.addObject("users", users);
		return modelAndView;
	} 
	//============================================================================================= addCollaboratorToSession
    @RequestMapping(value="sessions/addCollaboratorToSession", method = RequestMethod.POST)
	public ModelAndView addCollaboratorToSession( @RequestParam("sessionId") long sessionId , @RequestParam("collaboratorId") long collaboratorId){
		
    	System.out.println("entering ajax/user/addToSession");

		ModelAndView modelAndView = new ModelAndView("ajax/message");
		System.out.println("collaboratorId="+collaboratorId);
		System.out.println("sessionId="+sessionId);
		
		boolean success = sessionService.addCollaboratorToSession(collaboratorId , sessionId );
		modelAndView.addObject("message", "OK : user added to session successfully");
		return modelAndView;
	} 
	//============================================================================================= addTrainerToSession
    @RequestMapping(value="sessions/addTrainerToSession", method = RequestMethod.POST)
	public ModelAndView addTrainerToSession( @RequestParam("sessionId") long sessionId , @RequestParam("collaboratorId") long trainerId){
		
    	System.out.println("entering ajax/user/addToSession");

		ModelAndView modelAndView = new ModelAndView("ajax/message");
		System.out.println("trainerId="+trainerId);
		System.out.println("sessionId="+sessionId);
		
		boolean success = sessionService.addTrainerToSession(trainerId , sessionId );
		if(success == true)
			modelAndView.addObject("message", "OK : trainer added to session successfully");
		else
			modelAndView.addObject("message", "error : cannot add trainer to session");
		return modelAndView;
	} 
	//============================================================================================= remove collaborator from Session
    @RequestMapping(value="/collaborators/{collaboratorId}/delete", method = RequestMethod.POST)
    public  ModelAndView removeCollaboratorFromSession(@PathVariable("collaboratorId") int collaboratorId , @RequestParam("sessionId") int sessionId){
        
    	sessionService.removeCollaborator(sessionId,collaboratorId);
    	return new ModelAndView("ajax/message","message", "done");// : collaborator removed from session successfully");
    	//return "ajax/message";//redirect:/RF/sessions/"+sessionId;
    }
	//============================================================================================= remove trainer from Session
    @RequestMapping(value="sessions/removeTrainerFromSession", method = RequestMethod.POST)
    public ModelAndView removeTrainerFromSession(@RequestParam("sessionId") int sessionId){
        
    	ModelAndView modelAndView = new ModelAndView("ajax/message");
    	boolean success = sessionService.removeTrainer(sessionId);
		if(success == true)
			modelAndView.addObject("message", "OK : trainer removed from session successfully");
		else
			modelAndView.addObject("message", "error : cannot remove trainer from session");
		return modelAndView;
    }
	//============================================================================================= openSession
	@RequestMapping(value="sessions/{sessionId}/openSession", method = RequestMethod.POST)
    public ModelAndView openSession(@PathVariable("sessionId") int sessionId , HttpServletRequest request){//@ModelAttribute CheckboxList absenceList){

		ModelAndView modelAndView = new ModelAndView("ajax/message");
		//System.out.println(body);
		Session session = sessionDAO.getById(sessionId);

		if(session != null ) 
		{
			if(true)
			{
				session.setStatus(TrainingStatus.Open);
				//System.out.println("session.getStatus() "+session.getStatus());
				sessionDAO.update(session);
				if(true)//session.getStatus().equals(TrainingStatus.Open))
				{
			    	ApplicationContext context = 
			                new ClassPathXmlApplicationContext("/WEB-INF/spring-mail.xml");
			    
			       	SpringMailSender mm = (SpringMailSender) context.getBean("SpringMailSender");
			        String baseUrl = String.format("%s://%s:%d/tasks/",request.getScheme(),  request.getServerName(), request.getServerPort());

			       	Set<SessionCollaborator> sessionCollaborators = session.getSessionCollaborators();
			       	for (SessionCollaborator sessionCollaborator : sessionCollaborators) {
			       		
				           mm.sendMail("gformation2015@gmail.com",
					       		   "gformation2015@gmail.com",
					       		   "next session confirmation email", 
					       		   Editor.generateEmailBody(sessionCollaborator,baseUrl) 
					       		);
						
					}
			    
				}
				
			}
			modelAndView.addObject("message", "OK : session status has been set to Open successfully \n confirmation emails had been sent to collaboratorsemails .");
			return modelAndView;
		}

		modelAndView.addObject("error", "error :cannot set session status");
		return modelAndView;
    }
}
