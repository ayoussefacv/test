package com.GFormation.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.userdetails.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionCollaboratorDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.service.SessionService;


@Controller
public class CollaboratorsController {
	
	private static final Logger logger =LoggerFactory.getLogger(AdminController.class);

	@Autowired
	TrainingDao trainingDAO;
	@Autowired
	SessionDao sessionDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;
	@Autowired
	UserDao userDAO;
	@Autowired
	SessionService sessionService;
	@Autowired
	SessionCollaboratorDao sessionCollaboratorDao;


	//============================================================================================= list session collaborators
	@RequestMapping("sessions/{id}/collaborators")
    public ModelAndView list(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		Session session = this.sessionDAO.getById(id);
		if(session != null)
		{
			modelAndView.setViewName("session/show");
			Set<com.GFormation.core.model.User> collaborators = session.getCollaborators();
			modelAndView.addObject("session", session);
			modelAndView.addObject("collaborators", collaborators);
	        return modelAndView;
		}
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "no_session_found_with_this_id");
        return modelAndView;
    }
	//============================================================================================= list collaborator 
	@RequestMapping("collaborator/sessions")
    public ModelAndView list(){

		ModelAndView modelAndView = new ModelAndView();

	    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    
	    com.GFormation.core.model.User userd = userDAO.findUserByName(user.getUsername());
		List<Session> sessions = this.sessionService.getCollaboratorSessions(userd.getId());
		
    	System.out.println("list==="+sessions.toString());
    	
			modelAndView.setViewName("collaborator/sessions");
			modelAndView.addObject("sessions", sessions);
			
        return modelAndView;
    }
	//============================================================================================= show session 
	@RequestMapping("collaborator/sessions/{session_id}")
    public ModelAndView list(@PathVariable("session_id") long session_id){

		ModelAndView modelAndView = new ModelAndView();

		Session session =sessionDAO.getById(session_id);
    	System.out.println("list==="+session.toString());
    	
			modelAndView.setViewName("collaborator/session");
			modelAndView.addObject("session", session);
			
        return modelAndView;
    }
	//============================================================================================== get confirmation code
	@RequestMapping("/confirm/sessionId/{sessionId}/collaboratorId/{collaboratorId}/code/{confirmationCode}")
    public ModelAndView list(@PathVariable("sessionId") long sessionId,@PathVariable("collaboratorId") long collaboratorId,@PathVariable("confirmationCode") String confirmationCode){
		
		
		ModelAndView modelAndView = new ModelAndView();

		Session session =sessionDAO.getById(sessionId);
		SessionCollaborator sessionCollaborator =sessionCollaboratorDao.getByIds(sessionId,collaboratorId);
		
		if(sessionCollaborator!=null)
		{
			//System.out.println("## SessionCollaborator found ="+sessionCollaborator.toString());
			if(sessionCollaborator.getConfirmationCode().equals(confirmationCode))
			{
				sessionCollaborator.setConfirmed(true);
				//sessionCollaborator.setConfirmationCode("0");
				sessionCollaboratorDao.update(sessionCollaborator);
		        return new ModelAndView("message","message","your confirmation has been set successfully, login for more details");
				
			}
			
		}
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "not found");
        return modelAndView;
    }
	

}
