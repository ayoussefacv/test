package com.GFormation.core.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.config.SqlTimestampPropertyEditor;
import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Training;
import com.GFormation.core.util.Editor;
import com.GFormation.core.util.Validator;


@Controller
@RequestMapping(value="RF/trainings")
public class RF_TrainingController {
	
	private static final Logger logger =LoggerFactory.getLogger(RF_TrainingController.class);
	
	@Autowired
	TrainingDao trainingDAO;
	@Autowired
	SessionDao sessionDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;
	@Autowired	
	UserDao userDAO;

	//============================================================================================= Binder
	@InitBinder
	public void binder(WebDataBinder binder) {
		binder.registerCustomEditor(Timestamp.class,
	    new SqlTimestampPropertyEditor());
	}
	//============================================================================================= list
	@RequestMapping(value={""})
	public ModelAndView list(){
		
		logger.info("entering  /training");

		ModelAndView modelAndView = new ModelAndView("RF/training/list");
		
		List<Training> trainings = trainingDAO.getAll();
		modelAndView.addObject("trainings", trainings);
		return modelAndView;
	} 
	@RequestMapping("/{id}")
    public ModelAndView show(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		Training training = this.trainingDAO.getById(id);
		if(training != null)
		{
			Set<Session> sessions =training.getSessions();
			modelAndView.setViewName("RF/training/show");
			modelAndView.addObject("training", training);
			modelAndView.addObject("sessions", sessions);
	        return modelAndView;
		}
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "no_training_found_with_this_id");
        return modelAndView;
    }
	//============================================================================================= add
	@RequestMapping(value="/add")
	public ModelAndView addForm( ) {
		logger.info("form/add Page Method : GET");
		ModelAndView modelAndView = new ModelAndView("RF/training/add");
	    modelAndView.addObject("training", new Training());
	    
		return modelAndView;
	}

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public ModelAndView add(@Valid  @ModelAttribute("training") Training t , @RequestParam String date1 , @RequestParam String date2){
    	
    	System.out.println("####=== date1 = "+date2);
    	if(date1!="")
    	{
    		t.setStartDate(Editor.stringToDate(date1));
    		if(date2!="")
    			t.setEndDate(Editor.stringToDate(date2));
	        Training training=trainingDAO.add(t);
	        if(training!= null)
	        {   if(training.getId()!= 0)
	        	{
	    	        ModelAndView modelAndView = new ModelAndView("redirect:/RF/trainings/"+training.getId());
			        return modelAndView;
	        	}
	        }
			
    	}
	    ModelAndView modelAndView = new ModelAndView("RF/training/add");
	    modelAndView.addObject("error", "creating new training failed");
        return modelAndView;
    }

	//============================================================================================= update
	@RequestMapping("/{id}/update")
    public ModelAndView updateForm(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView("RF/training/update");
		modelAndView.addObject("training", this.trainingDAO.getById(id));
        return modelAndView;
    }

    @RequestMapping(value="/{id}/update", method = RequestMethod.POST)
    public ModelAndView update( @ModelAttribute("training") Training t, @RequestParam String date1 , @RequestParam String date2){
    	
    	if(date1!="")
    	{
    		System.out.println("date2=="+date2);
    		t.setStartDate(Editor.stringToDate(date1));
    		if(date2!="")
    			t.setEndDate(Editor.stringToDate(date2));

    		System.out.println("date2=="+t.getStartDate());
    		trainingDAO.update(t);
        	ModelAndView modelAndView = new ModelAndView("redirect:/RF/trainings/"+t.getId());
            return modelAndView;
    	}
	    ModelAndView modelAndView = new ModelAndView("RF/training/add");
	    modelAndView.addObject("error", "creating new training failed");
        return modelAndView;
    }


	//============================================================================================= delete
    @RequestMapping("/delete/{id}")
    public String removePerson(@PathVariable("id") int id){
         
    	trainingDAO.deleteById(id);
        return "redirect:/trainings";
    }

	//============================================================================================= add sessions
	
	@RequestMapping(value="/{id}/sessions/add")
	public ModelAndView addSessionForm(@PathVariable("id") int trainingId) {
		
		logger.info("trainings_manager Page Method : GET");

		ModelAndView modelAndView = new ModelAndView("RF/session/add");
	    modelAndView.addObject("training_id", trainingId);
		
		return modelAndView;
	}
    @RequestMapping(value="/{id}/sessions/add", method = RequestMethod.POST)
    public String add(@PathVariable("id") int id , @ModelAttribute("session") Session s, @RequestParam String date1,@RequestParam String time1, @RequestParam String time2){


    	if( Validator.session(s, date1, time1, time2))
    	{
	        Training training=trainingDAO.getById(id);
	        if(training!= null)
	        {
	        	//s.setTraining(training);
		        s=sessionDAO.add(s);
		        training.getSessions().add(s);
		        //trainingDAO.add(training);
		        return "redirect:/RF/sessions/"+s.getId();
	        }
    	}
        return "redirect:/RF/trainings/"+id+"/sessions/add";
    }
}
