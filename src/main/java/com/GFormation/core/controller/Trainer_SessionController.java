package com.GFormation.core.controller;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionCollaboratorDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.model.Training;
import com.GFormation.core.model.TrainingStatus;
import com.GFormation.core.service.SessionService;
import com.GFormation.core.util.Auth;
import com.GFormation.domain.CheckboxList;


@Controller
@RequestMapping(value="Trainer")
public class Trainer_SessionController {
	private static final Logger logger =LoggerFactory.getLogger(Trainer_SessionController.class);

	@Autowired
	TrainingDao trainingDAO;
	@Autowired
	SessionDao sessionDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;
	@Autowired
	UserDao userDAO;
	@Autowired
	SessionService sessionService;
	@Autowired
	SessionCollaboratorDao sessionCollaboratorDao;

	//============================================================================================= show
	@RequestMapping("sessions/{id}")
    public ModelAndView show(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		Session session = this.sessionDAO.getById(id);
		if(session != null ) 
		{

		    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		    com.GFormation.core.model.User userd = userDAO.findUserByName(user.getUsername());
		    System.out.println("userd1 ="+userd.toString());
		    System.out.println("userd2 ="+session.getTrainer().toString());
			if( Auth.getUser(userDAO).equals( session.getTrainer())) // verify if this trainer is the owner
			{
				System.out.println("yes its my session");
				modelAndView.setViewName("Trainer/session/show");
				modelAndView.addObject("session", session);
		        return modelAndView;
			}
			modelAndView.setViewName("error");
			modelAndView.addObject("message", "you do not have permession to see this session");
	        return modelAndView;
			
		}
		
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "no session found with this id");
        return modelAndView;
    }
	//============================================================================================= list
	@RequestMapping("sessions")
    public ModelAndView list(){

		ModelAndView modelAndView = new ModelAndView();

	    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    com.GFormation.core.model.User userd = userDAO.findUserByName(user.getUsername());
		List<Session> sessions = this.sessionService.getTrainerSessions(userd.getId());
		

    	System.out.println("list==="+sessions.toString());
    	
			modelAndView.setViewName("Trainer/session/list");
			modelAndView.addObject("sessions", sessions);
			
        return modelAndView;
    }

	//============================================================================================= add Documents
	//============================================================================================= setAbscenceList
		@RequestMapping(value="sessions/{sessionId}/setAbscenceList/{collaboratorId}/{isAbsent}", method = RequestMethod.POST)
	    public ModelAndView setAbscenceList(@PathVariable("sessionId") int sessionId ,@PathVariable("collaboratorId") int collaboratorId , @PathVariable("isAbsent") boolean isAbsent , @RequestBody String body){//@ModelAttribute CheckboxList absenceList){

			ModelAndView modelAndView = new ModelAndView("ajax/message");
			System.out.println(body);
			SessionCollaborator sessionCollaborator= new SessionCollaborator();
			com.GFormation.core.model.User collaborator = userDAO.getById(collaboratorId);
			Session session = sessionDAO.getById(sessionId);


			if(session != null ) 
			if(  Auth.getUser(userDAO).getId().equals(session.getTrainer().getId())) // verify if this trainer is the owner
			{
				if(collaborator!=null && session!=null )
				{
					sessionCollaborator.setCollaborator(collaborator);
					sessionCollaborator.setSession(session);
					sessionCollaborator.setAbsent(isAbsent);
					System.out.println("isAbsent====="+isAbsent);
					sessionCollaboratorDao.update(sessionCollaborator);
					
				}
				modelAndView.addObject("message", "OK : absence state is set successfully");
				return modelAndView;
			}

			modelAndView.addObject("message", "error :cannot set absence list");
			return modelAndView;
	    }



    
    
    
}
