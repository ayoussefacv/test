package com.GFormation.core.dao;

import java.util.List;

import com.GFormation.core.model.User;


public interface DAO<T> {

	T getById(long id);
	List<T> getAll();
	
	T add(T t);
	void update(T t);
    void deleteById(long id);
	void delete(T t);
    

}
