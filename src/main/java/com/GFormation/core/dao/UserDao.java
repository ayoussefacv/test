package com.GFormation.core.dao;

import java.util.List;

import com.GFormation.core.dao.impl.DAOImpl;
import com.GFormation.core.model.User;

public interface UserDao extends DAO<User>{

	User findUserByName(String username);

	List<User> searchByUsername(String username, int i, int j);

	com.GFormation.core.model.User getUserByUserName(String username);


}
