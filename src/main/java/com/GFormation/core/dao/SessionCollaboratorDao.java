package com.GFormation.core.dao;

import com.GFormation.core.model.SessionCollaborator;

public interface SessionCollaboratorDao   extends DAO<SessionCollaborator>{

	com.GFormation.core.model.SessionCollaborator getByIds(long sessionId,
			long collaboratorId);

}
