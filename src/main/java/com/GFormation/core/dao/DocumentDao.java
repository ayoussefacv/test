package com.GFormation.core.dao;

import java.util.List;

import com.GFormation.core.model.Document;
import com.GFormation.core.model.Session;

public interface DocumentDao extends DAO<Document>{
	  List<Document> listFiles();

	  Document getFile(Long id);

	  Document saveFile(Document uploadedFile);
	  
	  boolean isExist(Document document, Session session);
}
