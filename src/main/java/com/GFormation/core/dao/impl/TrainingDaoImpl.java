package com.GFormation.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.model.Training;

@Transactional
public class TrainingDaoImpl extends DAOImpl<Training> implements TrainingDao{

	public TrainingDaoImpl() {
		super(Training.class);
	}
	public TrainingDaoImpl(Class<Training> type) {
		super(type);
		// TODO Auto-generated constructor stub
	}



}