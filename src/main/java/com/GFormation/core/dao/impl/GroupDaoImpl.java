package com.GFormation.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.model.Group;

public class GroupDaoImpl extends DAOImpl<Group> implements GroupDao{

	public GroupDaoImpl(){
		super(Group.class);
	}

	public GroupDaoImpl(Class<Group> type) {
		super(type);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Group getByTitle(String title) {
		Criteria criteria = getCurrentSession().createCriteria(Group.class);
		criteria.add(Restrictions.eq("title", title));		
		return  (Group) criteria.uniqueResult();
	}



}