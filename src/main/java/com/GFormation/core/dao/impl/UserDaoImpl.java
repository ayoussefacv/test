package com.GFormation.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.GFormation.core.dao.DAO;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.User;

public class UserDaoImpl extends DAOImpl<User> implements UserDao{

	public UserDaoImpl(){
		super(User.class);
	}

	public User findUserByName(String username) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));		
		User user= (User) criteria.uniqueResult();
		//user.getRoles();
		//System.out.println("DaoImpl : user1="+user.getRoles());
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchByUsername(String username, int init, int max) {
		return getCurrentSession().createCriteria(User.class)
			    .add( Restrictions.like("username", "%"+username+"%"))
			    .setFirstResult(init)
				.setMaxResults(max)
			    .list();
	}

	@Override
	public User getUserByUserName(String username) {
		// TODO Auto-generated method stub
		return findUserByName(username);
	}



}
