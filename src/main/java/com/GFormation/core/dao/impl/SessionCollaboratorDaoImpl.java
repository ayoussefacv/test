package com.GFormation.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;







import com.GFormation.core.dao.SessionCollaboratorDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.model.User;

public class SessionCollaboratorDaoImpl extends DAOImpl<SessionCollaborator> implements SessionCollaboratorDao{

	public SessionCollaboratorDaoImpl() {
		super(SessionCollaborator.class);
	}
	public SessionCollaboratorDaoImpl(Class<SessionCollaborator> type) {
		super(type);
		// TODO Auto-generated constructor stub
	}

	public SessionCollaborator getByIds(long sessionId,long collaboratorId)
	{
		Criteria criteria = getCurrentSession().createCriteria(SessionCollaborator.class);
		criteria.add(Restrictions.eq("pk.session.id", sessionId));		
		criteria.add(Restrictions.eq("pk.collaborator.id", collaboratorId));		
		return  (SessionCollaborator) criteria.uniqueResult();
		
	}
	public void delete(SessionCollaborator sessionCollaborator)
	{
		
		Query query = getCurrentSession().createSQLQuery("DELETE FROM Session_Collaborators WHERE (session_id=? and collaborator_id=?)");
		query.setLong(0, sessionCollaborator.getSession().getId());
		query.setLong(1, sessionCollaborator.getCollaborator().getId());
		 
		int result = query.executeUpdate();
		System.out.println("result"+result);
	}
//	public void setCollaboratorAbsence(Session session,User collaborator,boolean isAbsent)
//	{
//		
//		Query query = getCurrentSession().createSQLQuery("UPDATE FROM Session_Collaborators WHERE (session_id=? and collaborator_id=?)");
//		query.setLong(0, session.getId());
//		query.setLong(1, collaborator.getId());
//		 
//		int result = query.executeUpdate();
//		System.out.println("result"+result);
//	}
	
	



}