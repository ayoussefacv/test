package com.GFormation.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.GFormation.core.model.Document;
import com.GFormation.core.model.User;
import com.GFormation.core.dao.DocumentDao;

public class DocumentDaoImpl extends DAOImpl<Document> implements DocumentDao{

	public DocumentDaoImpl() {
		super(Document.class);
	}
	public DocumentDaoImpl(Class<Document> type) {
		super(type);
		// TODO Auto-generated constructor stub
	}
	  @Autowired
	  private SessionFactory sessionFactory;

	  @SuppressWarnings("unchecked")
	  public List<Document> listFiles() {
	    return getSession().createCriteria(Document.class).list();
	  }

	  public Document getFile(Long id) {
	    return (Document) getSession().get(Document.class, id);
	  }

	  private Session getSession() {
	    Session sess = getSessionFactory().getCurrentSession();
	    if (sess == null) {
	      sess = getSessionFactory().openSession();
	    }
	    return sess;
	  }

	  public Document saveFile(Document uploadedFile) {
	    return (Document) getSession().merge(uploadedFile);

	  }

	  private SessionFactory getSessionFactory() {
	    return sessionFactory;
	  }
	  
	@Override
	public boolean isExist(Document document,com.GFormation.core.model.Session session) {
		
		int size= getSession().createQuery("from Document where name=? and session_id = ?")
			.setString(0,document.getName())
			.setLong(1,session.getId()).list().size();
		if(size>0)
			return true;
		return false;
	}


}