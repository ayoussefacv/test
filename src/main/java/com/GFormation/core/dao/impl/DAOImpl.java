package com.GFormation.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.GFormation.core.dao.DAO;
import com.GFormation.core.model.User;

import org.hibernate.Session;


@Transactional
public abstract class DAOImpl<T> implements DAO<T> {

	   private Class< T > clazz;
	   @Autowired
	   SessionFactory sessionFactory;

	   public DAOImpl(  ){
	   }
	   public DAOImpl( Class< T > clazzToSet ){
		      this.clazz = clazzToSet;
		   }
	 
	   public T getById( long id ){
	      return (T) getCurrentSession().get( clazz, id );
	   }
	   public List< T > getAll(){
	      return getCurrentSession().createQuery( "from " + clazz.getName() ).list();
	   }
	 
	   public T add( T entity ){
	      getCurrentSession().persist( entity );
	      return entity;
	   }
	 
	   public void update( T entity ){
	      getCurrentSession().merge( entity );
	   }

	   public void delete( T entity ){
	      getCurrentSession().delete( entity );
	   }
	   
	   public void deleteById( long entityId ){
	      T entity = getById( entityId );
	      delete( entity );
	   }
	 
	   protected final Session getCurrentSession(){
	      return sessionFactory.getCurrentSession();
	   }
}
