package com.GFormation.core.model;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Version;

import javax.persistence.Table;
import java.lang.Override;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.GFormation.core.model.Session;

import java.util.Set;
import java.util.HashSet;

import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="Training")
public class Training implements Serializable
{

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id = null;
   //@Version
   @Column(name = "version")
   private int version = 0;

   @Column(length = 30)
   private String title;

   @Column(length = 1000)
   private String description;

   @DateTimeFormat(pattern="yyyy-MM-dd")
   @Temporal(TemporalType.DATE)
   private Date startDate;

   @DateTimeFormat(pattern="yyyy-MM-dd")
   @Temporal(TemporalType.DATE)
   private Date endDate;
   
	@Column(name="status",length = 10)
	@Enumerated(EnumType.STRING)
	private TrainingStatus status = TrainingStatus.OnHold;

	public TrainingStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingStatus status) {
		this.status = status;
	}
   @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
   @JoinColumn(name="training_id")
   private Set<Session> sessions = new HashSet<Session>();
   
   public Set<Session> getSessions() {
	   return sessions;
   }
	
	public void setSessions(Set<Session> sessions) {
		this.sessions = sessions;
	}
	
	public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object that)
   {
      if (this == that)
      {
         return true;
      }
      if (that == null)
      {
         return false;
      }
      if (getClass() != that.getClass())
      {
         return false;
      }
      if (id != null)
      {
         return id.equals(((Training) that).id);
      }
      return super.equals(that);
   }

   @Override
   public int hashCode()
   {
      if (id != null)
      {
         return id.hashCode();
      }
      return super.hashCode();
   }

   public String getTitle()
   {
      return this.title;
   }

   public void setTitle(final String title)
   {
      this.title = title;
   }

   public String getDescription()
   {
      return this.description;
   }

   public void setDescription(final String description)
   {
      this.description = description;
   }

   public Date getStartDate()
   {
      return this.startDate;
   }

   public void setStartDate(final Date startDate)
   {
      this.startDate = startDate;
   }

   public Date getEndDate()
   {
      return this.endDate;
   }
   public void setStartTime(final Date startDate)
   {
      this.startDate = startDate;
   }

   public Date getEndTime()
   {
      return this.endDate;
   }

   public void setEndDate(final Date endDate)
   {
      this.endDate = endDate;
   }

   @Override
   public String toString()
   {
      String result = getClass().getSimpleName() + " ";
      if (title != null && !title.trim().isEmpty())
         result += "title: " + title;
      if (description != null && !description.trim().isEmpty())
         result += ", description: " + description;
      return result;
   }

}