package com.GFormation.core.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Embeddable
public class SessionCollaboratorId implements Serializable {

	private Session session;
    private User collaborator;

	@Override
	public String toString() {
		return "SessionCollaboratorId [session=" + session + ", collaborator="
				+ collaborator + "]";
	}

	@ManyToOne(fetch=FetchType.LAZY)
	public Session getSession() {
		return session;
	}
 
	public void setSession(Session session) {
		this.session = session;
	}
 
	@ManyToOne(fetch=FetchType.LAZY )
	public User getCollaborator() {
		return collaborator;
	}

	public void setCollaborator(User collaborator) {
		this.collaborator = collaborator;
	}
 

	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
 
        SessionCollaboratorId that = (SessionCollaboratorId) o;
 
        if (session != null ? !session.equals(that.session) : that.session != null) return false;
        if (collaborator != null ? !collaborator.equals(that.collaborator) : that.collaborator != null)
            return false;
 
        return true;
    }
 
 
    public int hashCode() {
        int result;
        result = (session != null ? session.hashCode() : 0);
        result = 31 * result + (collaborator != null ? collaborator.hashCode() : 0);
        return result;
    }
 
}
