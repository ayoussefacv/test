package com.GFormation.core.model;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import java.lang.Override;

import com.GFormation.core.model.Role;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.OneToMany;


@Entity
@Table(name="User")
public class User implements Serializable
{

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id = null;
   //@Version
   @Column(name = "version")
   private int version = 0;

   @Column(length = 30)
   private String username;

   @Column
   private String password;

   @Column(length = 30)
   private String email;
   
   @Column(length = 30)
   private String firstName;
   
   @Column(length = 30)
   private String lastName;

	//============================================ status
	@Column(name="status",length = 10)
	@Enumerated(EnumType.STRING)
	private UserStatus status = UserStatus.ACTIVE;

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}
//============================================ status end
   
   @ManyToOne
   @JoinColumn(name="group_id", 
   				insertable=true, updatable=true, 
   				nullable=true)
   private Group group ;
   
//   @OneToMany(cascade=CascadeType.ALL,mappedBy="pk.collaborator",fetch=FetchType.EAGER)
//   private Set<SessionCollaborator> sessions = new HashSet<SessionCollaborator>();
//
//    public Set<SessionCollaborator> getSessions() {
//		return sessions;
//	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

//	public void setSessions(Set<SessionCollaborator> sessions) {
//		this.sessions = sessions;
//	}

   public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object that)
   {
      if (this == that)
      {
         return true;
      }
      if (that == null)
      {
         return false;
      }
      if (getClass() != that.getClass())
      {
         return false;
      }
      if (id != null)
      {
         return id.equals(((User) that).id);
      }
      return super.equals(that);
   }

   @Override
   public int hashCode()
   {
      if (id != null)
      {
         return id.hashCode();
      }
      return super.hashCode();
   }

   public String getUsername()
   {
      return this.username;
   }

   public void setUsername(final String username)
   {
      this.username = username;
   }
	public String getFirstName() {
	return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
   public String getPassword()
   {
      return this.password;
   }

   public void setPassword(final String password)
   {
      this.password = password;
   }

   public String getEmail()
   {
      return this.email;
   }

   public void setEmail(final String email)
   {
      this.email = email;
   }


	@Override
	public String toString() {
		return "User [id=" + id + ", version=" + version + ", username=" + username
				+ ", password=" + password + ", email=" + email + ", firstName="
				+ firstName + ", lastName=" + lastName + ", status=" + status
				+ ", group=" + group + "]";
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
}