package com.GFormation.core.model;


import java.io.Serializable;
import java.util.Date;
 

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
@Table(name = "Session_collaborators")
@AssociationOverrides({
	@AssociationOverride(name = "pk.session", 
		joinColumns = @JoinColumn(name = "session_id")),
	@AssociationOverride(name = "pk.collaborator", 
		joinColumns = @JoinColumn(name = "collaborator_id")) })

public class SessionCollaborator implements Serializable{

	private SessionCollaboratorId pk = new SessionCollaboratorId();
	private Date createdDate;
	private boolean absent;
	private boolean confirmed;
	private String confirmationCode;
 
	public SessionCollaborator() {
	}
 
	@EmbeddedId
	public SessionCollaboratorId getPk() {
		return pk;
	}
 
	public void setPk(SessionCollaboratorId pk) {
		this.pk = pk;
	}
 
	@Override
	public String toString() {
		return "SessionCollaborator [pk=" + pk.toString() + ", createdDate=" + createdDate
				+ ", absent=" + absent + ", confirmed=" + confirmed + "]";
	}

	@Transient
	public Session getSession() {
		return getPk().getSession();
	}
 
	public void setSession(Session session) {
		getPk().setSession(session);
	}
 
	@Transient
	public User getCollaborator() {
		return getPk().getCollaborator();
	}
 
	public void setCollaborator(User collaborator) {
		getPk().setCollaborator(collaborator);
	}
 
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE", nullable = true, length = 10)
	public Date getCreatedDate() {
		return this.createdDate;
	}
 
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


    //===================================================== absent
	@Column( nullable = true)
	public boolean isAbsent() {
		return absent;
	}		
	@Transient
	public boolean getAbsent() {
		return absent;
	}

	public void setAbsent(boolean absent) {
		this.absent = absent;
	}
    //===================================================== confirmed
	@Transient
	public boolean getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	@Column( nullable = true)
	public boolean isConfirmed() {
		return confirmed;
	}
    //===================================================== confirmation code

	@Column( nullable = true)
	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}
	//======================================================
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
 
		SessionCollaborator that = (SessionCollaborator) o;
 
		if (getPk() != null ? !getPk().equals(that.getPk())
				: that.getPk() != null)
			return false;
 
		return true;
	}
 
	public int hashCode() {
		return (getPk() != null ? getPk().hashCode() : 0);
	}

	
	
    
}
