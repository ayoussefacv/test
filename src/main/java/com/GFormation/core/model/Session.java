package com.GFormation.core.model;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import java.lang.Override;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.OneToMany;

import com.GFormation.core.model.Document;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "Session")
public class Session implements Serializable
{
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id = null;
   //@Version
   @Column(name = "version")
   private int version = 0;

   @Column(length = 50)
   private String title;

   @Column(length = 1000)
   private String description;

   @Temporal(TemporalType.DATE)
   private Date startDate;

   @Temporal(TemporalType.DATE)
   private Date endDate;

   @Temporal(TemporalType.TIME)
   private Date startTime;

   @Temporal(TemporalType.TIME)
   private Date endTime;
   
	@Column(name="status",length = 10)
	@Enumerated(EnumType.STRING)
	private TrainingStatus status = TrainingStatus.OnHold;

	public TrainingStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingStatus status) {
		this.status = status;
	}
	
   @OneToMany(cascade=CascadeType.ALL , fetch=FetchType.EAGER)
   @JoinColumn(name="session_id")
   @JsonBackReference
   private Set<Document> documents = new HashSet<Document>();

   
   @OneToMany(cascade=CascadeType.ALL , mappedBy="pk.session", fetch=FetchType.EAGER)
   private Set<SessionCollaborator>  sessionCollaborators = new HashSet<SessionCollaborator>();
   @Transient
   private Set<User> collaborators = new HashSet<User>();
   
   public void addCollaborator(User collaborator) {
	    SessionCollaborator association = new SessionCollaborator();
	    association.setCollaborator(collaborator);
	    association.setSession(this);
	    association.setAbsent(true);
	    this.sessionCollaborators.add(association);
	    // Also add the association object to the employee.
	    //collaborator.getSessions().add(association);
   }
   public void removeCollaborator(User collaborator) {
		    SessionCollaborator association = new SessionCollaborator();
		    association.setCollaborator(collaborator);
		    association.setSession(this);
		    System.out.println("###SessionCollaborator="+association.toString());
		    //SessionCollaborator s = null;
		    for(SessionCollaborator sessionCollaborator:sessionCollaborators)
		    {
		    	if(sessionCollaborator.equals(association))
		    	{
		    		//System.out.println("###b1 contains="+sessionCollaborators.contains(sessionCollaborator));
		    		//s=sessionCollaborator;
		    		this.sessionCollaborators.remove(sessionCollaborator);
		    		//System.out.println("###b2 contains="+sessionCollaborators.contains(sessionCollaborator));
		    	}
		    	
		    }
		    //boolean success=sessionCollaborators.contains(s);
		    //System.out.println("###success="+success);
		    // Also add the association object to the employee.
		    //collaborator.getSessions().remove(association);
   }
   public void echo()
   {
	   System.out.println("######### echo SessionCollaborators");
	   for(SessionCollaborator sessionCollaborator:sessionCollaborators)
	    {
		   System.out.println("###sessionCollaborator="+sessionCollaborator.toString());
	    }
   }
   @ManyToOne
   @JoinColumn(name="training_id", 
   				insertable=false, updatable=false, 
   				nullable=true)
   private Training training ;
   
   @ManyToOne
   @JoinColumn(name="trainer_id")
   private User trainer ;// = new SessionCollaborator();
   
   	public Training getTraining() {
	   return training;
	}
	
	public void setTraining(Training training) {
		this.training = training;
	}

	public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object that)
   {
      if (this == that)
      {
         return true;
      }
      if (that == null)
      {
         return false;
      }
      if (getClass() != that.getClass())
      {
         return false;
      }
      if (id != null)
      {
         return id.equals(((Session) that).id);
      }
      return super.equals(that);
   }

   @Override
   public int hashCode()
   {
      if (id != null)
      {
         return id.hashCode();
      }
      return super.hashCode();
   }

   public String getTitle()
   {
      return this.title;
   }

   public void setTitle(final String title)
   {
      this.title = title;
   }

   public String getDescription()
   {
      return this.description;
   }

   public void setDescription(final String description)
   {
      this.description = description;
   }

   public Date getStartDate()
   {
      return this.startDate;
   }

   public void setStartDate(final Date startDate)
   {
      this.startDate = startDate;
   }

   public Date getEndDate()
   {
      return this.endDate;
   }

   public void setEndDate(final Date endDate)
   {
      this.endDate = endDate;
   }

	   @Override
	public String toString() {
		return "Session [id=" + id + ", version=" + version + ", title=" + title
				+ ", description=" + description + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", status=" + status 
				+ ", trainer=" + trainer + "]";
	}

public Set<Document> getDocuments()
   {
      return this.documents;
   }

   public void setDocuments(final Set<Document> documents)
   {
      this.documents = documents;
   }
   //====================================================================
   public Set<SessionCollaborator> getSessionCollaborators() {
	return sessionCollaborators;
   }
   public Set<User> getCollaborators() {
	   collaborators = new HashSet<User>();
	   for(SessionCollaborator sessionCollaborator : this.getSessionCollaborators()){
		   collaborators.add(sessionCollaborator.getCollaborator());
	   }
	return collaborators;
   }

	public void setSessionCollaborators(Set<SessionCollaborator> sessionCollaborators) {
		this.sessionCollaborators = sessionCollaborators;
	}

	public User getTrainer() {
		return trainer;
	}
	
	public void setTrainer(User trainer) {
		this.trainer = trainer;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


}