package com.GFormation.core.util;
 
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
 








import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Training;
import com.GFormation.core.model.User;
 
public class DbUtil {

	@Autowired
	private UserDao userDao;
	@Autowired
	private GroupDao groupDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private TrainingDao trainingDao;
	
	public void initialize(){
		if(groupDao.getById(1) == null)
		{
			System.out.println("####################### initialising DB :");

			Group groupAdmins = new Group("groupAdmins");
			Group groupManagers = new Group("groupManagers");
			Group groupTrainers = new Group("groupTrainers");
			Group groupCollaborators = new Group("groupCollaborators");

			groupAdmins.getRoles().add(new Role("ROLE_ADMIN"));
			groupManagers.getRoles().add(new Role("ROLE_MANAGER"));
			groupTrainers.getRoles().add(new Role("ROLE_TRAINER"));
			groupCollaborators.getRoles().add(new Role("ROLE_COLLABORATOR"));

			groupDao.add(groupAdmins);
			groupDao.add(groupManagers);
			groupDao.add(groupTrainers);
			groupDao.add(groupCollaborators);

			User user=new User();
			
			user.setUsername("admin");
			user.setPassword("e10adc3949ba59abbe56e057f20f883e");
			user.setEmail("admin@admin.com");
			user.setFirstName("admin1");
			user.setLastName("admin1");
			user.setGroup(groupAdmins);
			userDao.add(user);

			user=new User();
			user.setUsername("manager");
			user.setPassword("e10adc3949ba59abbe56e057f20f883e");
			user.setFirstName("manager1");
			user.setLastName("manager1");
			user.setEmail("manager@manager.com");
			user.setGroup(groupManagers);
			userDao.add(user);
			
			user=new User();
			user.setUsername("trainer");
			user.setPassword("e10adc3949ba59abbe56e057f20f883e");
			user.setFirstName("trainer1");
			user.setLastName("trainer1");
			user.setEmail("trainer@trainer.com");
			user.setGroup(groupTrainers);
			userDao.add(user);
			
			user=new User();
			user.setUsername("collaborator");
			user.setPassword("e10adc3949ba59abbe56e057f20f883e");
			user.setFirstName("collaborator1");
			user.setLastName("collaborator1");
			user.setEmail("collaborator@collaborator.com");
			user.setGroup(groupCollaborators);
			userDao.add(user);


			Date date1 = Editor.stringToDate("2015-02-01");
			Date date2 = Editor.stringToDate("2015-06-30");

			Date time1 = Editor.stringToTime("09:00");
			Date time2 = Editor.stringToTime("12:00");
			
			Training training = new Training();
			training.setTitle("training 1");
			training.setStartDate(date1);
			training.setEndDate(date2);
			training.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");
			
			Session session = new Session();
			session.setTitle("session 1");
			session.setStartTime(time1);
			session.setEndTime(time2);
			session.setEndDate(date1);
			session.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");

			Set<Session> sessions = new HashSet<Session>();
			
			Session session2 = new Session();
			session2.setTitle("session 2");
			session2.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");

			sessions.add(session);
			sessions.add(session2);
			training.setSessions(sessions);
			
			trainingDao.add(training);
			
			System.out.println("####################### end initialising DB :");
		}
	
	}
}