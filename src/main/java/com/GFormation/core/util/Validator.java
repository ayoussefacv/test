package com.GFormation.core.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.config.SqlTimestampPropertyEditor;
import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Training;


@Controller
@RequestMapping(value="RF/trainings")
public final class Validator {

	public static boolean session(Session s ,String date1 , String time1 , String time2)
	{
    	if(time1!="" && time2!="" && date1!="")
    	{
    		System.out.println("date1=="+date1);
    		System.out.println("time1=="+time1);
    		System.out.println("time2=="+time2);
    		
    		s.setStartDate(Editor.stringToDate(date1));
    		s.setStartTime(Editor.stringToTime(time1));
    		s.setEndTime(Editor.stringToTime(time2));
    		return true;
    	}
		return false;
	}
}