<%@ include file="/WEB-INF/jsp/common/header.jsp" %>

	<h1>Training ${session.training.id }</h1>
	<ul>
		<li>session_id : ${session.id}</li>
		<li>title : ${session.title}</li>
		<li>description : ${session.description}</li>
		<li>startDate : ${session.startDate}</li>
		<li>endDate${session.endDate}</li>
	</ul>
	<h2>List of sessions</h2>
	<hr />
	<a href="${base_url}/RF/sessions/${session.id}/update">update session</a>
	<table border="1" width="50%"  id="listSesssionUsers">
		<tr>
			<div class="contentArea">
				<input type="text" class="search" id="inputSearchUser" /> put username<br /> 
				<div id="divResultSearchUser">
				</div>
			</div>
		</tr>
		<tr>
			<th>username</th>
			<th>email</th>
			<th>status</th>
			<th>options</th>
		</tr>
		
		<div >
			<sw:forEach items="${session.collaborators}" var="o" >
				<tr class="element" element-id="${o.id}">
					<td>${o.username }</td>
					<td>${o.email }</td>
					<td>${o.status }</td>
					<td>
						<a href="${base_url}/RF/sessions/${session.id}/collaborators/${o.id}/delete">delete</a>
					</td>
				</tr>
			</sw:forEach>
		</div>
	</table>
	<table border="1" width="50%"  id="listSesssionUsers">
		<tr>
			<div class="upload">
				<input type="text" class="search" id="inputSearchUser" /> upload file<br /> 
			</div>
		</tr>
		<tr>
			<th>username</th>
			<th>email</th>
			<th>status</th>
			<th>options</th>
		</tr>
		
		<div >
			<sw:forEach items="${session.documents}" var="o" >
				<tr class="element" element-id="${o.id}">
					<td>${o.name }</td>
					<td>${o.url }</td>
					<td>
						<a href="${base_url}/documents/${o.url}">download</a>
						<a href="${base_url}/documents/${o.id}/delete">delete</a>
					</td>
				</tr>
			</sw:forEach>
		</div>
	</table>



	<script type="text/javascript">
			var session=new Object();
				session.id=${session.id};

	</script>
</body>
</html>