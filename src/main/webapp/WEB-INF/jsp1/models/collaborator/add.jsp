<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sw" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<spring:url value="${base_url}" var="base_url" htmlEscape="true"/>
<html>
<head>
    <title>Add</title>
</head>
<body>

<h2>New training</h2>
<form method="POST" action="${base_url}/RF/trainings/add">
   <table>
    <tr>
        <td><label path="title">title</label></td>
        <td><input name="title" /></td>
    </tr>
    <tr>
        <td><label path="description">description</label></td>
        <td><input name="description" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form>
</body>
</html>