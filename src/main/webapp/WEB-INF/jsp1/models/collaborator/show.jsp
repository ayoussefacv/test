<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sw" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<ul>
		<li>title : ${session.title}</li>
		<li>description : ${session.description}</li>
		<li>startDate : ${session.startDate}</li>
		<li>endDate : ${session.endDate}</li>
	</ul>
	<h1>List of sessions</h1>
	<hr />
	<br />
	<a href="${base_url}/RF/sessions/update/${session.id}">update session</a>
	<a href="${base_url}/RF/sessions/${session.id }/RF/sessions/add">Add new session to this session</a>
	<table border="1" width="50%">
		<tr>
			<th>title</th>
			<th>description</th>
			<th>startDate</th>
			<th>endDate</th>
			<th>collaborateurs</th>
			<th>options</th>
		</tr>
		
		<!--  listeProduit  -->
		<sw:forEach items="${session.sessions}" var="o" >
			<tr>
				<td><a href="${base_url}/RF/sessions/${o.id}">${o.title }</a></td>
				<td>${o.description }</td>
				<td>${o.startDate }</td>
				<td>${o.endDate }</td>
				<td><a href="${base_url}/RF/sessions/${o.id}/collaborators">collaborators</a></td>
				<td>
					<a href="${base_url}/RF/sessions/update/${o.id}">update</a>
					<a href="${base_url}/RF/sessions/delete/${o.id}">delete</a>
				</td>
			</tr>
		</sw:forEach>
	</table>
</body>
</html>