<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Add</title>
</head>
<body>

<h2>Update training</h2>
<form method="POST" action="${base_url}/RF/trainings/update/${training.id}">
   <table>
   		<input type="hidden" name="id" value="${training.id}" />
    <tr>
        <td><label for="title">title</label></td>
        <td><input name="title" value="${training.title}" /></td>
    </tr>
    <tr>
        <td><label for="description">description</label></td>
        <td><input name="description" value="${training.description}"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form>
</body>
</html>