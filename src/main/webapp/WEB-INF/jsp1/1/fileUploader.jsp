<%@ include file="common/header.jsp" %>

<link rel="stylesheet" type="text/css" href='${resources_url}/libs/bootstrap-3.1.1/css/bootstrap.min.css'>
<link rel="stylesheet" type="text/css" href='${resources_url}/libs/bootstrap-dialog/css/bootstrap-dialog.min.css'>
<link rel="stylesheet" type="text/css" href='${resources_url}/css/style.css'>


	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h3>Spring MVC + Dropzone.js Example</h3>
			</div>
			<div class="panel-body">
				<div>
					<form id="dropzone-form" class="dropzone"
						enctype="multipart/form-data">

						<div
							class="dz-default dz-message file-dropzone text-center well col-sm-12">

							<span class="glyphicon glyphicon-paperclip"></span> <span>
								To attach files, drag and drop here</span><br> <span>OR</span><br>
							<span>Just Click</span>
						</div>

						<!-- this is were the previews should be shown. -->
						<div class="dropzone-previews"></div>
					</form>
					<hr>
					<button id="upload-button" class="btn btn-primary">
						<span class="glyphicon glyphicon-upload"></span> Upload
					</button>
					<a class="btn btn-primary pull-right" href="list">
						<span class="glyphicon glyphicon-eye-open"></span> View All Uploads
					</a>
				</div>
			</div>
		</div>
	</div>
	<script type='text/javascript' src='${resources_url}/libs/jquery/jquery-2.1.1.js'></script>
	<script type='text/javascript' src='${resources_url}/libs/bootstrap-3.1.1/js/bootstrap.js'></script>
	<script type='text/javascript' src='${resources_url}/libs/bootstrap-dialog/js/bootstrap-dialog.min.js'></script>
	<script type='text/javascript' src='${resources_url}/libs/dropzone.js'></script>
	<script type='text/javascript' src='${resources_url}/js/app.js'></script>
	
