<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sw" uri="http://java.sun.com/jsp/jstl/core" %> 
  
<sw:set value="${pageContext.request.contextPath}" var="base_url"/>
<sw:set value="${pageContext.request.contextPath}/resources/special/fileUpload" var="resources_url"/>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>jQuery File Upload Example</title>
<script src="${resources_url}/js/jquery.1.9.1.min.js"></script>

<script src="${resources_url}/js/vendor/jquery.ui.widget.js"></script>
<script src="${resources_url}/js/jquery.iframe-transport.js"></script>
<script src="${resources_url}/js/jquery.fileupload.js"></script>

<!-- bootstrap just to have good looking page -->
<script src="${resources_url}/bootstrap/js/bootstrap.min.js"></script>
<link href="${resources_url}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />

<!-- we code these -->
<link href="${resources_url}/css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="${resources_url}/js/myuploadfunction.js"></script>
</head>

<body>
<h1>Spring MVC - jQuery File Upload</h1>
<div style="width:500px;padding:20px">

	<input id="fileupload" type="file" name="files[]" data-url="rest/controller/upload" multiple>
	
	<div id="dropzone" class="fade well">Drop files here</div>
	
	<div id="progress" class="progress">
    	<div class="bar" style="width: 0%;"></div>
	</div>

	<table id="uploaded-files" class="table">
		<tr>
			<th>File Name</th>
			<th>File Size</th>
			<th>File Type</th>
			<th>Download</th>
		</tr>
	</table>
	
</div>
</body> 
</html>
