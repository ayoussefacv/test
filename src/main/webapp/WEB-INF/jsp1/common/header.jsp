<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sw" uri="http://java.sun.com/jsp/jstl/core" %> 
  
<sw:set value="${pageContext.request.contextPath}" var="base_url"/>
<sw:set value="${pageContext.request.contextPath}/resources" var="resources_url"/>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<script type="text/javascript" src="${resources_url}/special/lib/jquery-1.8.0.min.js"></script>
	<script type="text/javascript"> var base_url = "${base_url}";</script>
	<link href="${resources_url}/special/autocomplete/getUserInfo.css" rel="stylesheet" />
</head>
<body>