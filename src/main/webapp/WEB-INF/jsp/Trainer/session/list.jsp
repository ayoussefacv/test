<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<%@ include file="/WEB-INF/jsp/common/sidebar.jsp" %>


	<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Blank Page <small>subtitle here</small></h1>
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="#">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							
							
							
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<h1>your sessions</h1>
										<table class="table table-striped table-bordered table-hover" id="projects">
											<thead>
											<tr>
												<th>Title</th>
												<th>Date</th>
												<th>From</th>
												<th>To</th>
												<th class="hidden-xs center">Status</th>
												<th></th>
											</tr>
											</thead>
											<tbody>
											<sw:forEach items="${sessions}" var="o" >
												<tr  class="sessionLigne" element-id="${o.id}">
													<td>${o.title }</td>
													<td>${o.startDate}</td>
													<td>${o.startTime}</td>
													<td>${o.endTime}</td>
													<td class="center hidden-xs"><span class="label label-danger">${o.status}</span></td>
													<td class="center">
														<div class="visible-md visible-lg hidden-sm hidden-xs">
															<a href="${base_url}/Trainer/sessions/${o.id}" class="btn btn-green tooltips" data-placement="top" data-original-title="see more"><i class="fa fa-share"></i></a>
														</div>
														<div class="visible-xs visible-sm hidden-md hidden-lg">
															<div class="btn-group">
																<a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
																	<i class="fa fa-cog"></i> <span class="caret"></span>
																</a>
																<ul role="menu" class="dropdown-menu dropdown-dark pull-right">
																	
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="${base_url}/Trainer/sessions/${o.id}">
																			<i class="fa fa-share"></i> See
																		</a>
																	</li>
																</ul>
															</div>
														</div></td>
												</tr>
									</sw:forEach>
											</tbody>
										</table>

									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

<%@ include file="/WEB-INF/jsp/common/footer_2.jsp" %>


		<script>
			jQuery(document).ready(function() {
				
				
				UIModals.init("session");

			});
		</script>

</body>
</html>













