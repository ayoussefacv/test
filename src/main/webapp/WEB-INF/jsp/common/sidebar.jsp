<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sw" uri="http://java.sun.com/jsp/jstl/core" %> 
  
<sw:set value="${pageContext.request.contextPath}" var="base_url"/>
<sw:set value="${pageContext.request.contextPath}/resources" var="resources_url"/>




			<!-- start: PAGESLIDE LEFT -->
			<a class="closedbar inner hidden-sm hidden-xs" href="#">
			</a>
			<nav id="pageslide-left" class="pageslide inner">
				<div class="navbar-content">
					<!-- start: SIDEBAR -->
					<div class="main-navigation left-wrapper transition-left">
						<div class="navigation-toggler hidden-sm hidden-xs">
							<a href="#main-navbar" class="sb-toggle-left">
							</a>
						</div>
						<div class="user-profile border-top padding-horizontal-10 block">
							<div class="inline-block">
								<img width="45px" src="${resources_url}/assets/images/anonymous.jpg" alt="">
							</div>
							<div class="inline-block">
								<h5 class="no-margin"> Bienvenu </h5>
								<h4 class="no-margin"> Mr. Admin </h4>

							</div>
						</div>
						<!-- start: MAIN NAVIGATION MENU -->
						<ul class="main-navigation-menu">
							<li>
								<a href="index.html"><i class="fa fa-home"></i> <span class="title"> Acceuil </span><span class="label label-default pull-right ">Home</span> </a>
							</li>
							<li>
								<a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Administration </span><i class="icon-arrow"></i> </a>
								<ul class="sub-menu">
									<li>
										<a href="javascript:;">
											Utilisateurs <i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Ajouter
												</a>
											</li>

										</ul>
									</li>
									<li>
										<a href="javascript:;">
											Groups <i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Ajouter
												</a>
											</li>

										</ul>
									</li>

								</ul>
							</li>

							<li class="active open">
								<a href="javascript:void(0)"><i class="fa fa-code"></i> <span class="title">Formations</span><i class="icon-arrow"></i> </a>
								<ul class="sub-menu">
									<li>
										<a href="pages_user_profile.html">
											<span class="title">Liste Formations</span>
										</a>
									</li>
								</ul>
							</li>

						</ul>
						<!-- end: MAIN NAVIGATION MENU -->
					</div>
					<!-- end: SIDEBAR -->
				</div>
				<div class="slide-tools">
					<div class="col-xs-6 text-left no-padding">

					</div>
					<div class="col-xs-6 text-right no-padding">
						<a class="btn btn-sm log-out text-right" href="${base_url}/login?logout">
							<i class="fa fa-power-off"></i> Log Out
						</a>
					</div>
				</div>
			</nav>
			<!-- end: PAGESLIDE LEFT -->