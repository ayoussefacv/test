<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<%@ include file="/WEB-INF/jsp/common/sidebar.jsp" %>


<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Blank Page <small>subtitle here</small></h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12">
								<a href="#" class="back-subviews">
									<i class="fa fa-chevron-left"></i> BACK
								</a>
								<a href="#" class="close-subviews">
									<i class="fa fa-times"></i> CLOSE
								</a>
								<div class="toolbar-tools pull-right">
									<!-- start: TOP NAVIGATION MENU -->
									<ul class="nav navbar-right">
										<!-- start: TO-DO DROPDOWN -->
										<li class="dropdown">
											<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
												<i class="fa fa-plus"></i> SUBVIEW
												<div class="tooltip-notification hide">
													<div class="tooltip-notification-arrow"></div>

												</div>
											</a>
											<ul class="dropdown-menu dropdown-light dropdown-subview">
												<li class="dropdown-header">
													Notes
												</li>
												<li>
													<a href="#newNote" class="new-note"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new note</a>
												</li>
												<li>
													<a href="#readNote" class="read-all-notes"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Read all notes</a>
												</li>
												<li class="dropdown-header">
													Calendar
												</li>
												<li>
													<a href="#newEvent" class="new-event"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new event</a>
												</li>
												<li>
													<a href="#showCalendar" class="show-calendar"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Show calendar</a>
												</li>
												<li class="dropdown-header">
													Contributors
												</li>
												<li>
													<a href="#newContributor" class="new-contributor"><span class="fa-stack"> <i class="fa fa-user fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new contributor</a>
												</li>
												<li>
													<a href="#showContributors" class="show-contributors"><span class="fa-stack"> <i class="fa fa-user fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Show all contributor</a>
												</li>
											</ul>
										</li>

									</ul>
									<!-- end: TOP NAVIGATION MENU -->
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="#">
											Dashboard
										</a>
									</li>
									<li class="active">
										Blank Page
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<div >

											<h3>Ajouter un utilisateur</h3>
											<form method="POST" action="${base_url}/admin/users_manager" class="">
												<div class="row">
													<div class="col-md-12">
														<div class="errorHandler alert alert-danger no-display">
															<i class="fa fa-times-sign"></i> You have some form errors. Please check below.
														</div>
														<div class="successHandler alert alert-success no-display">
															<i class="fa fa-ok"></i> Your form validation is successful!
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<input class="contributor-id hide" type="text">
															<label class="control-label">
																Prenom  <span class="symbol required"></span>
															</label>
															<spring:bind path="user.firstName">
															<input type="text" placeholder="Inserez Votre Prenom" class="form-control contributor-firstname" required="true" name="firstName">
															</spring:bind>
														</div>
														<div class="form-group">
															<label class="control-label">
																Nom <span class="symbol required"></span>
															</label>
															<spring:bind path="user.lastName">
															<input type="text" placeholder="Inserez Votre Nom" class="form-control contributor-lastname" required="true" name="lastName">
															</spring:bind>
														</div>
														<div class="form-group">
															<label class="control-label">
																Username <span class="symbol required"></span>
															</label>
															<spring:bind path="user.username">
															<input type="text" placeholder="Inserez Votre Nom" class="form-control contributor-lastname" required="true" name="username">
															</spring:bind>
														</div>
														<div class="form-group">
															<label class="control-label">
																Email Address <span class="symbol required"></span>
															</label>
															<spring:bind path="user.email">
															<input type="email" placeholder="Inserez Votre Email" class="form-control contributor-email" name="email">
															</spring:bind>
														</div>
														<div class="form-group">
															<label class="control-label">
																Password <span class="symbol required"></span>
															</label>
															<spring:bind path="user.password">
															<input type="password" class="form-control contributor-password" name="password">
															</spring:bind>
														</div>
														<div class="form-group">
															<label class="control-label">
																Confirm Password <span class="symbol required"></span>
															</label>
															<input type="password" class="form-control contributor-password-again" name="password_again">
														</div>
													</div>
													<div class="col-md-6">
														
														<div class="form-group">
															<label class="control-label">
																Type de Profile <span class="symbol required"></span>
															</label>
															
															<select name="group_id" class="form-control contributor-permits" >
																<option value="1">Administrateur</option>
																<option value="2">Responsable Formation</option>
																<option value="3">Formateur</option>
																<option value="4">Colloborateur</option>
															</select>
														
															
														</div>
														
													</div>
												</div>
												<div class="pull-right">
													<div class="btn-group">
														<button class="btn btn-info " type="submit">
															Enregister
														</button>
													</div>
												</div>
											</form>

										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->


<%@ include file="/WEB-INF/jsp/common/footer.jsp" %>









