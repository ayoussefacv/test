<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<%@ include file="/WEB-INF/jsp/common/sidebar.jsp" %>
<!-- ============================================================================================ -->
<script type="text/javascript">
		 var base_url = "${base_url}";
		 var session = new Object();
		 session.id = ${session.id};
</script>
<!-- ============================================================================================ -->

			<!-- start: PAGESLIDE RIGHT -->
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h2>${session.title}</h2>
									<a href="${base_url}/RF/sessions/${session.id}/update">update session</a>
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<br/>
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-sm-12">
								<div class="tabbable">
									<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
										<li class="active">
											<a data-toggle="tab" href="#panel_overview">
												overview
											</a>
										</li>
										<li>
											<a data-toggle="tab" href="#panel_trainer">
												Trainer
											</a>
										</li>
										<li>
											<a data-toggle="tab" href="#panel_collaborators">
												collaborators
											</a>
										</li>
										<li>
											<a data-toggle="tab" href="#panel_update">
												update
											</a>
										</li>
									</ul>
									<div class="tab-content">
										<div id="panel_overview" class="tab-pane fade in active">
											<div class="row">
												<div class="col-sm-5 col-md-4">
													<div class="user-left">
														<table class="table table-condensed table-hover">
															<thead>
															<tr>
																<th colspan="3">details</th>
															</tr>
															</thead>
															<tbody>
															<tr>
																<td>title</td>
																<td>${session.title}</td>
																<td><a href="#panel_update" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
															</tr>
															<tr>
																<td>start date</td>
																<td>${session.startDate}</td>
																<td><a href="#panel_update" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
															</tr>
															<tr>
																<td>start time</td>
																<td>${session.startTime}</td>
																<td><a href="#panel_update" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
															</tr>
															<tr>
																<td>end time</td>
																<td>${session.endTime}</td>
																<td><a href="#panel_update" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
															</tr>
															<tr>
																<td>status
																	<sw:if test="${session.status == 'OnHold'}">
																			(click to open this session)
																			<sw:set var="btnColor" value="btn-info"/>
																	</sw:if>
																	<sw:if test="${session.status == 'Open'}">
																			<sw:set var="btnColor" value="btn-success"/>
																	</sw:if>
																</td>
																<td><button id="sessionStatus" class="btn ${btnColor}" sessionStatus="${session.status}">${session.status}
																</button></td>

															</tr>
															</tbody>
														</table>
													</div>
												</div>
												<div class="col-sm-7 col-md-8">
																
																<b>description</b>
																<p>${session.description}</p>
												</div>
											</div>
										</div>
										<div id="panel_trainer" class="tab-pane fade">
													<label>
														...
													</label>
													<div class="">
														<%@ include file="/WEB-INF/jsp/modules/addTrainerToSession.jsp" %>
													</div>
										</div>
										<div id="panel_collaborators" class="tab-pane fade">
													<label>
														...
													</label>
													<div class="">
														<%@ include file="/WEB-INF/jsp/modules/addCollaboratorsToSession.jsp" %>
													</div>
											
										</div>
										<div id="panel_update" class="tab-pane fade">
													<label>
														...
													</label>
													<form method="POST" action="${base_url}/RF/sessions/${session.id}/update" id="form255">
														<%@ include file="/WEB-INF/jsp/modules/session/update.jsp" %>
													</form>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					
	<%@ include file="/WEB-INF/jsp/common/footer_2.jsp" %>

	<script type='text/javascript' src='${resources_url}/libs/bootstrap-dialog/js/bootstrap-dialog.min.js'></script>
	<script type='text/javascript' src='${resources_url}/libs/dropzone.js'></script>
	<script type='text/javascript' src='${resources_url}/js/app.js'></script>
	

	<script type="text/javascript" src="${resources_url}/special/autocomplete/getTrainerInfo.js"></script>
	<script type="text/javascript" src="${resources_url}/special/autocomplete/getUserInfo.js"></script>
	<script type="text/javascript" src="${resources_url}/special/other/sessionStatus.js"></script>
	
		<script>
			jQuery(document).ready(function() {
				
				
				UIModals.init("collaborator");

			});
		</script>
	</body>
	<!-- end: BODY -->
</html>