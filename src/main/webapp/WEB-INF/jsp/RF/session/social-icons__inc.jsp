															<div class="social-icons block">
																<ul>
																	<li data-placement="top" data-original-title="Twitter" class="social-twitter tooltips">
																		<a href="http://www.twitter.com" target="_blank">
																			Twitter
																		</a>
																	</li>
																	<li data-placement="top" data-original-title="Facebook" class="social-facebook tooltips">
																		<a href="http://facebook.com" target="_blank">
																			Facebook
																		</a>
																	</li>
																	<li data-placement="top" data-original-title="Google" class="social-google tooltips">
																		<a href="http://google.com" target="_blank">
																			Google+
																		</a>
																	</li>
																	<li data-placement="top" data-original-title="LinkedIn" class="social-linkedin tooltips">
																		<a href="http://linkedin.com" target="_blank">
																			LinkedIn
																		</a>
																	</li>
																	<li data-placement="top" data-original-title="Github" class="social-github tooltips">
																		<a href="#" target="_blank">
																			Github
																		</a>
																	</li>
																</ul>
															</div>