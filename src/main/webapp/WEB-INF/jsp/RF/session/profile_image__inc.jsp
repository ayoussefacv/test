<div class="user-profile border-top padding-horizontal-10 block">
							<div class="inline-block">
								<img src="${resources_url}/assets/images/avatar-1.jpg" alt="">
							</div>
							<div class="inline-block">
								<h5 class="no-margin"> Welcome </h5>
								<h4 class="no-margin"> ${session.trainer.lastName} ${session.trainer.firstName} </h4>
								<a class="btn user-options sb_toggle">
									<i class="fa fa-cog"></i>
								</a>
							</div>
						</div>