<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<%@ include file="/WEB-INF/jsp/common/sidebar.jsp" %>



			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Blank Page <small>subtitle here</small></h1>
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="#">
											Dashboard
										</a>
									</li>
									<li class="active">
										<a class="btn btn-success" href="${base_url}/RF/trainings/add">Add new training</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
						
						<sw:forEach items="${trainings}" var="training" >		
						
							<div class="col-md-6 col-lg-4 col-sm-6">
								<div class="panel panel-default panel-white core-box">
									<div class="panel-tools">
										<a href="#" class="btn btn-xs btn-link panel-close">
											<i class="fa fa-times"></i>
										</a>
									</div>
									<div class="panel-body no-padding">
										<div class="partition-green padding-20 text-center core-icon">
											<i class="fa fa-university fa-2x icon-big"></i>
										</div>
										
										<div class="padding-20 core-content">
											<b class="title block  no-margin">${training.title}</b><br/>
											<span class="subtitle"><b>from </b>${training.startDate }<br/>

															<sw:if test="${not empty training.endDate}">
												     			<b>to</b> ${training.endDate }
												     		</sw:if>

											 </span>
										</div>
									</div>
									<div class="panel-footer clearfix no-padding">
										<div class=""></div>
										<a class="col-xs-4 padding-10 text-center text-white tooltips " data-toggle="tooltip" data-placement="top" ></a>
										<a class="col-xs-4 padding-10 text-center text-white tooltips " data-toggle="tooltip" data-placement="top" ></a>
										
										<a href="${base_url}/RF/trainings/${training.id}" class="col-xs-4 padding-10 text-center text-white tooltips partition-red" data-toggle="tooltip" data-placement="top" title="View More"><i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
							</div>
							
					</sw:forEach>
					
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			
			<%@ include file="/WEB-INF/jsp/common/footer_2.jsp" %>
		<script>
			jQuery(document).ready(function() {
				UIModals.init("training");

			});
		</script>
	