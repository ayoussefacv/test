<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<%@ include file="/WEB-INF/jsp/common/sidebar.jsp" %>
<fmt:formatDate value="${yourObject.date}" var="dateString" pattern="dd/MM/yyyy" />

<!-- start: MAIN CONTAINER -->
            <div class="main-container inner">
                <!-- start: PAGE -->
                <div class="main-content">
                    <!-- start: PANEL CONFIGURATION MODAL FORM -->
                    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title">Panel Configuration</h4>
                                </div>
                                <div class="modal-body">
                                    Here will be a configuration form
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                    <button type="button" class="btn btn-primary">
                                        Save changes
                                    </button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                    <div class="container">
                        <!-- start: PAGE HEADER -->
                        <!-- start: TOOLBAR -->
                        <div class="toolbar row">
                            <div class="col-sm-6 hidden-xs">
                                <div class="page-header">
                                    <h1>Blank Page <small>subtitle here</small></h1>
                                </div>
                            </div>
                        </div>
                        <!-- end: TOOLBAR -->
                        <!-- end: PAGE HEADER -->
                        <!-- start: BREADCRUMB -->
                        <div class="row">
                            <div class="col-md-12">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="#">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li class="active">
                                        Blank Page
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- end: BREADCRUMB -->
                        <!-- start: PAGE CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-body">
                                        <div >

                                            <h3>Add new training</h3>
                                            <form method="POST" action="${base_url}/RF/trainings/add" id="form255">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="errorHandler alert alert-danger no-display">
                                                            <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                        </div>
                                                        <div class="successHandler alert alert-success no-display">
                                                            <i class="fa fa-ok"></i> Your form validation is successful!
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    
                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="text">
                                                            <label class="control-label">
                                                                training title  <span class="symbol required"></span>
                                                            </label>
                                                            <input type="text" placeholder="Insert a title" class="form-control" name="title" required="true">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="date">
                                                            <label class="control-label">
                                                                from  <span class="symbol required"></span>
                                                            </label>
                                                            <input type="date" placeholder="" class="form-control datepicker" name="date1" required="true">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="date">
                                                            <label class="control-label">
                                                                to  <span class="symbol"></span>
                                                            </label>
                                                            <input type="date" placeholder="" class="form-control datepicker" name="date2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div>
                                                            <label for="form-field-24">
                                                                Description
                                                            </label>
                                                            <textarea  name="Description" class="autosize form-control" id="form-field-24" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 134px;"></textarea>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="btn-group">
                                                        <button class="btn btn-info save-contributor" type="submit">
                                                            Enregister
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end: PAGE CONTENT-->
                    </div>
                    <div class="subviews">
                        <div class="subviews-container"></div>
                    </div>
                </div>
                <!-- end: PAGE -->
            </div>
            <!-- end: MAIN CONTAINER -->


        <%@ include file="/WEB-INF/jsp/common/footer_2.jsp" %>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.datepicker').datepicker();
            };
        </script>
        </body>
    <!-- end: BODY -->
</html>









