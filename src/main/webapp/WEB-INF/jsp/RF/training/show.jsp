<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<%@ include file="/WEB-INF/jsp/common/sidebar.jsp" %>


	<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Blank Page <small>subtitle here</small></h1>
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="#">
											Dashboard
										</a>
									</li>
									<li class="active">
										
										<a class="btn btn-success" href="${base_url}/RF/trainings/${training.id}/update">update training</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							
							<div class="col-md-12">
								<div class="panel panel-default panel-white core-box">
									<div class="panel-tools">
										<a href="#" class="btn btn-xs btn-link panel-close">
											<i class="fa fa-times"></i>
										</a>
									</div>
									<div class="panel-body no-padding">
										<div class="partition-green padding-20 text-center core-icon">
											<i class="fa fa-university fa-2x icon-big"></i>
										</div>
										<div class="padding-20 core-content">
										<h1>${training.title}</h1>
											<span class="subtitle"><b>from </b>${training.startDate }<br/>

															<sw:if test="${not empty training.endDate}">
												     			<b>to</b> ${training.endDate }
												     		</sw:if>

											 </span><br/>
											 <span class="subtitle">
											 	${training.description}
											 	
											 </span>
										</div>
									</div>
									<div class="panel-footer clearfix no-padding">
										<div class=""></div>
										<a class="col-xs-4 padding-10 text-center text-white tooltips " data-toggle="tooltip" data-placement="top" ></a>
										<a class="col-xs-4 padding-10 text-center text-white tooltips " data-toggle="tooltip" data-placement="top" ></a>
										<a href="${base_url}/RF/trainings" class="col-xs-4 padding-10 text-center text-white tooltips partition-red" data-toggle="tooltip" data-placement="top" title="return to trainings page"><i class="fa fa-chevron-left"></i></a>
									</div>
								</div>
							</div>
							
							
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<a class="btn btn-success" href="${base_url}/RF/trainings/${training.id }/sessions/add">Add new session to this training</a>
										<table class="table table-striped table-bordered table-hover" id="projects">
											<thead>
											<tr>
												<th>Title</th>
												<th class="hidden-xs">Trainer</th>
												<th>Date</th>
												<th>From</th>
												<th>To</th>
												<th class="hidden-xs center">Status</th>
												<th></th>
											</tr>
											</thead>
											<tbody>
											<sw:forEach items="${training.sessions}" var="o" >
												<tr  class="sessionLigne" element-id="${o.id}">
													<td>${o.title }</td>
													<td class="hidden-xs">${o.trainer.lastName} ${o.trainer.firstName}</td>
													<td>${o.startDate}</td>
													<td>${o.startTime}</td>
													<td>${o.endTime}</td>
													<td class="center hidden-xs"><span class="label label-danger">${o.status}</span></td>
													<td class="center">
														<div class="visible-md visible-lg hidden-sm hidden-xs">
															<a href="${base_url}/RF/sessions/${o.id}/update" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
															<a href="${base_url}/RF/sessions/${o.id}" class="btn btn-green tooltips" data-placement="top" data-original-title="see more"><i class="fa fa-share"></i></a>
															<button  data-bb="confirm" class="btn btn-red tooltips" data-placement="top" data-original-title="Remove" element-id="${o.id}"><i class="fa fa-times fa fa-white"></i></button>
														</div>
														<div class="visible-xs visible-sm hidden-md hidden-lg">
															<div class="btn-group">
																<a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
																	<i class="fa fa-cog"></i> <span class="caret"></span>
																</a>
																<ul role="menu" class="dropdown-menu dropdown-dark pull-right">
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="${base_url}/RF/sessions/${o.id}/update">
																			<i class="fa fa-edit"></i> Edit
																		</a>
																	</li>
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="${base_url}/RF/sessions/${o.id}">
																			<i class="fa fa-share"></i> See
																		</a>
																	</li>
																	<li role="presentation">
																		<a role="menuitem" tabindex="-1" href="${base_url}/RF/sessions/${o.id}/delete">
																			<i class="fa fa-times"></i> Remove
																		</a>
																	</li>
																</ul>
															</div>
														</div></td>
												</tr>
									</sw:forEach>
											</tbody>
										</table>

									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

<%@ include file="/WEB-INF/jsp/common/footer_2.jsp" %>


		<script>
			jQuery(document).ready(function() {
				
				
				UIModals.init("session");

			});
		</script>

</body>
</html>















<!---
	<a href="${base_url}/RF/trainings/${training.id}/update">update training</a>
	<a href="${base_url}/RF/trainings/${training.id }/sessions/add">Add new session to this training</a>
--->