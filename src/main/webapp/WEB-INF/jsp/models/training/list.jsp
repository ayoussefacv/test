<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
	<h1>List</h1>

	
	recherche : 
	<form action="${base_url}/RF/trainings/search" method="post"  >
		ID : <input type="text" name="title" /> 
		<input type="submit" value="Afficher" />
	</form>
	<hr/>


	<a href="${base_url}/RF/trainings/add">Add new training</a>
	<hr />
	<br />
	<table border="1" width="50%">
		<tr>
			<th>title</th>
			<th>description</th>
			<th>startDate</th>
			<th>endDate</th>
			<th>sessions</th>
			<th>options</th>
		</tr>
		
		<!--  listeProduit  -->
		<sw:forEach items="${trainings}" var="o" >
			<tr>
				<td><a href="${base_url}/RF/trainings/${o.id}">${o.title }</a></td>
				<td>${o.description }</td>
				<td>${o.startDate }</td>
				<td>${o.endDate }</td>
				<td><a href="${base_url}/RF/trainings/${o.id }">sessions</a></td>
				<td>
					<a href="${base_url}/RF/trainings/${o.id}/update">update</a>
					<a href="${base_url}/RF/trainings/${o.id}/delete">delete</a>
				</td>
			</tr>
		</sw:forEach>
	</table>
</body>
</html>