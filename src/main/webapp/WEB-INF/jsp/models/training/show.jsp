<%@ include file="/WEB-INF/jsp/common/header.jsp" %>

	<ul>
		<li>title : ${training.title}</li>
		<li>description : ${training.description}</li>
		<li>startDate : ${training.startDate}</li>
		<li>endDate : ${training.endDate}</li>
	</ul>
	<h1>List of sessions</h1>
	<hr />
	<br />
	<a href="${base_url}/RF/trainings/${training.id}/update">update training</a>
	<a href="${base_url}/RF/trainings/${training.id }/RF/sessions/add">Add new session to this training</a>
	<table border="1" width="50%">
		<tr>
			<th>title</th>
			<th>description</th>
			<th>startDate</th>
			<th>endDate</th>
			<th>collaborators</th>
			<th>options</th>
		</tr>
		
		<!--  listeProduit  -->
		<sw:forEach items="${training.sessions}" var="o" >
			<tr>
				<td><a href="${base_url}/RF/sessions/${o.id}">${o.title }</a></td>
				<td>${o.description }</td>
				<td>${o.startDate }</td>
				<td>${o.endDate }</td>
				<td><a href="${base_url}/RF/sessions/${o.id}/collaborators">collaborators</a></td>
				<td>
					<a href="${base_url}/RF/sessions/${o.id}/update">update</a>
					<a href="${base_url}/RF/sessions/${o.id}/delete">delete</a>
				</td>
			</tr>
		</sw:forEach>
	</table>
</body>
</html>