<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Update</title>
</head>
<body>

<h2>Update session</h2>
<form method="POST" action="${base_url}/RF/sessions/update/${session.id}">
   <table>
        <input type="hidden" name="id" value="${session.id}" />
    <tr>
        <td><label for="title">title</label></td>
        <td><input name="title" value="${session.title}" /></td>
    </tr>
    <tr>
        <td><label for="description">description</label></td>
        <td><input name="description" value="${session.description}"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form>
</body>
</html>