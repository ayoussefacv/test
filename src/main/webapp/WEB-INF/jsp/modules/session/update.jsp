                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="errorHandler alert alert-danger no-display">
                                                            <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                        </div>
                                                        <div class="successHandler alert alert-success no-display">
                                                            <i class="fa fa-ok"></i> Your form validation is successful!
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    	<input type="hidden" name="id" value="${session.id}">

                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="text">
                                                            <label class="control-label">
                                                                session title  <span class="symbol required"></span>
                                                            </label>
                                                            <input type="text" placeholder="Insert a title" class="form-control" name="title" required="true"  value="${session.title}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="date">
                                                            <label class="control-label">
                                                                day  <span class="symbol required"></span>
                                                            </label>
                                                            <input type="date" placeholder="" class="form-control datepicker" name="date1" required="true"  value="${session.startDate}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="time">
                                                            <label class="control-label">
                                                                from  <span class="symbol"></span>
                                                            </label>
                                                            <input type="time" placeholder="" class="form-control" name="time1"  value="${session.startTime}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="contributor-id hide" type="time">
                                                            <label class="control-label">
                                                                to  <span class="symbol"></span>
                                                            </label>
                                                            <input type="time" placeholder="" class="form-control" name="time2"  value="${session.endTime}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div>
                                                            <label for="form-field-24">
                                                                Description
                                                            </label>
                                                            <textarea name="description" class="autosize form-control" id="form-field-24" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 134px;">${session.description}</textarea>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="btn-group">
                                                        <button class="btn btn-info save-contributor" type="submit">
                                                            Enregister
                                                        </button>
                                                    </div>
                                                </div>