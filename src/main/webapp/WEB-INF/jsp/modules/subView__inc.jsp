
                            <div class="col-sm-6 col-xs-12">
                                <a href="#" class="back-subviews">
                                    <i class="fa fa-chevron-left"></i> BACK
                                </a>
                                <a href="#" class="close-subviews">
                                    <i class="fa fa-times"></i> CLOSE
                                </a>
                                <div class="toolbar-tools pull-right">
                                    <!-- start: TOP NAVIGATION MENU -->
                                    <ul class="nav navbar-right">
                                        <!-- start: TO-DO DROPDOWN -->
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                                <i class="fa fa-plus"></i> SUBVIEW
                                                <div class="tooltip-notification hide">
                                                    <div class="tooltip-notification-arrow"></div>

                                                </div>
                                            </a>
                                            <ul class="dropdown-menu dropdown-light dropdown-subview">
                                                <li class="dropdown-header">
                                                    Notes
                                                </li>
                                                <li>
                                                    <a href="#newNote" class="new-note"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new note</a>
                                                </li>
                                                <li>
                                                    <a href="#readNote" class="read-all-notes"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Read all notes</a>
                                                </li>
                                                <li class="dropdown-header">
                                                    Calendar
                                                </li>
                                                <li>
                                                    <a href="#newEvent" class="new-event"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new event</a>
                                                </li>
                                                <li>
                                                    <a href="#showCalendar" class="show-calendar"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Show calendar</a>
                                                </li>
                                                <li class="dropdown-header">
                                                    Contributors
                                                </li>
                                                <li>
                                                    <a href="#newContributor" class="new-contributor"><span class="fa-stack"> <i class="fa fa-user fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new contributor</a>
                                                </li>
                                                <li>
                                                    <a href="#showContributors" class="show-contributors"><span class="fa-stack"> <i class="fa fa-user fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Show all contributor</a>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                    <!-- end: TOP NAVIGATION MENU -->
                                </div>
                            </div>