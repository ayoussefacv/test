	<div class="row" id="collaboratorsListContainer">

		<div class="contentArea col-sm-5 col-md-4">
			<div class="col-sm-12">
				<span class="input-icon">
					<input  type="text"  id="inputSearchUser" placeholder="put username" id="form-field-14" class="form-control search">
					<i class="fa fa-user"></i> </span>
			</div>
			<div id="divResultSearchUser" class="col-sm-12">
			</div>
		</div>

		<div class="contentArea col-sm-7 col-md-8">
			<table class="table table-striped table-bordered table-hover" id="listSesssionUsers">
				<thead>
				<tr>
					<th class="center"></th>
					<th>Full Name</th>
					<th class="hidden-xs">Username</th>
					<th class="hidden-xs">Email</th>
					<th class="hidden-xs">confirmation</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<sw:forEach items="${session.sessionCollaborators}" var="o" >
						<tr  class="element" element-id="${o.collaborator.id}">
							
							<td class="center"><img src="${resources_url}/assets/images/anonymous.jpg" alt="image" style="width:25px; height:25px;"/></td>
							<td>${o.collaborator.lastName} ${o.collaborator.firstName}</td>
							<td class="hidden-xs">${o.collaborator.username}</td>
							<td class="hidden-xs">
								<a href="#" rel="nofollow" target="_blank">
									${o.collaborator.email }
								</a></td>
							<td class="hidden-xs">

												<a class="isConfirmed" val="${o.confirmed}">
														<sw:choose>
														    <sw:when test="${o.confirmed == true}">
														        confirmed
														    </sw:when>
														    <sw:otherwise>
														        not yet
														    </sw:otherwise>
														</sw:choose>
												</a>

							</td>
							<td class="center">
								<div class="visible-md visible-lg hidden-sm hidden-xs">
									<button  data-bb="confirm" class="btn btn-red tooltips" data-placement="top" data-original-title="Remove" edit-element-id="${o.collaborator.id}" edit-element-idd="${session.id}"><i class="fa fa-times fa fa-white"></i></button>
								</div>
								</td>
						</tr>
					</sw:forEach>
				</tbody>
			</table>
		</div>
		<!-- ====================================================================================================== -->

	</div>

