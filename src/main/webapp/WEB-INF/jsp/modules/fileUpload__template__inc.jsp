														<div data-provides="fileupload" class="fileupload fileupload-new">
														<span class="btn btn-file btn-light-grey"><i class="fa fa-folder-open-o"></i> <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
															<input type="file">
														</span>
															<span class="fileupload-preview"></span>
															<a data-dismiss="fileupload" class="close fileupload-exists float-none" href="#">
																&times;
															</a>
														</div>
														<p class="help-block">
															<button class="btn btn-icon btn-block">
																Upload
																<i class="clip-list-3"></i>

															</button>
														</p>