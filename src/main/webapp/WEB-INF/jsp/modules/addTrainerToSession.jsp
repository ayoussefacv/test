	<div class="row" id="getTrainerContainer">


		<div class="contentArea col-sm-5 col-md-4">
			<div class="col-sm-12">
				<span class="input-icon">
					<input  type="text"  id="inputSearchTrainer" placeholder="put username" id="form-field-14" class="form-control search">
					<i class="fa fa-user"></i> </span>
			</div>

			<div class="col-sm-12" id="divResultSearchTrainer">
			</div>
		</div>



		<div class="contentArea col-sm-7 col-md-8">
			<table border="1" width="50%"  id="listSesssionTrainer">
				<tr>
					<th>username</th>
					<th>email</th>
					<th>status</th>
					<th>options</th>
				</tr>
				
				<sw:choose>
				    <sw:when test="${session.trainer != null}">
						<tr class="element" element-id="${session.trainer.id}">
							<td>${session.trainer.username }</td>
							<td>${session.trainer.email }</td>
							<td>${session.trainer.status }</td>
							<td>
								<button id="buttonRemoveTrainer">removeTrainer</button>
							</td>
						</tr>
				    </sw:when>
				</sw:choose>
			</table>
		</div>
	</div>