$(function(){
	var listedUsers;
	var listedUsersIds;
	$.fn.liveOn = function (event, callback) {
  		$(document).on(event, $(this).selector, callback);  
	}
	$("#listSesssionTrainer .element").each(function(index,row) 
	{
		var user = new Object();
		user.id = $(this).attr('element-id');
		listedUsers=user;
		listedUsersIds=user.id;

		console.log(user.id);
	});
	$("#inputSearchTrainer").keyup(function() 
	{ 
		var inputSearch = $(this).val();
		var dataString = 'username='+ inputSearch;
		if(inputSearch!='')
		{
			$.ajax({
			type: "POST",
			url: base_url+"/ajax/user/search",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$("#divResultSearchTrainer").html(html).show();
			}
			});
		}
		return false;    
	});

	$("#divResultSearchTrainer").liveOn("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		var decoded = $("<div/>").html($name).text();
		$('#inputSearchTrainer').val(decoded);
	});

	$("#getTrainerContainer .display_box").liveOn("click",function(e){ 

		var newUser = new Object();

		newUser.id = $(this).attr('element-id');
		newUser.username = $(this).find('.name').text();
		newUser.email = $(this).find('.email').text();
		newUser.status = $(this).find('.status').text();

		console.log("clicked user id ="+newUser.id);
		console.log("clicked user username ="+newUser.username);


		$('#inputSearchTrainer').val(newUser.username);
		$("#divResultSearchTrainer").fadeOut(); 

		if(newUser.id !=listedUsersIds)
		{
			// add user to list :
			var dataString = 'sessionId='+ session.id +'&collaboratorId='+ newUser.id;
			if(newUser.id!='')
			{
				$.ajax({
				type: "POST",
				url: base_url+"/ajax/user/addTrainerToSession",
				data: dataString,
				cache: false,
				success: function(html)
				{
					$("#listSesssionTrainer .element").remove();
					$("#listSesssionTrainer").append(rowTemplate(newUser));
					console.log(html);
				}
				});
			}
			listedUsersIds= newUser.id;

		}
		return false; 

	});
	$(document).liveOn("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
		$("#divResultSearchTrainer").fadeOut(); 
		}
	});
	$('#inputSearchUser').click(function(){
		$("#divResultSearchTrainer").fadeIn();
	});

	var rowTemplate = function(user)
	{
		var htmlr='<tr class="element" style="background:rgba(0,255,0,0.3)">'+
					'<td>'+user.username+'</td>'+
					'<td>'+user.email+'</td>'+
					'<td>'+user.status+'</td>'+
					'<td>'+
						'<button id="buttonRemoveTrainer">removeTrainer</button>'+
					'</td>'+
				'</tr>';
		return htmlr;
	}

	$("#buttonRemoveTrainer").liveOn("click",function(){
			var dataString = 'sessionId='+ session.id;
			if(1)
			{
				$.ajax({
				type: "POST",
				url: base_url+"/RF/sessions/removeTrainerFromSession",
				data: dataString,
				cache: false,
				success: function(html)
				{
					$("#listSesssionTrainer .element").remove();
					console.log(html);
				}
				});
			}
	});
});